part of 'page_bloc.dart';

abstract class PageEvent extends Equatable {
  const PageEvent();
}

class GoToSplashPage extends PageEvent {
  @override
  List<Object> get props => [];
}

class GoToSignInPage extends PageEvent {
  @override
  List<Object> get props => [];
}

class GoToMainPage extends PageEvent {
  final int bottomNavBarIndex;

  GoToMainPage({this.bottomNavBarIndex = 0});

  @override
  List<Object> get props => [bottomNavBarIndex];
}

class GoToChatPage extends PageEvent {
  final PageEvent pageEvent;

  GoToChatPage(this.pageEvent);

  @override
  List<Object> get props => [pageEvent];
}

class GoToEditProfilePage extends PageEvent {
  final PageEvent pageEvent;

  GoToEditProfilePage(this.pageEvent);

  @override
  List<Object> get props => [pageEvent];
}

class GoToChangePasswordPage extends PageEvent {
  final PageEvent pageEvent;

  GoToChangePasswordPage(this.pageEvent);

  @override
  List<Object> get props => [pageEvent];
}

class GoToAddProductPage extends PageEvent {
  final PageEvent pageEvent;

  GoToAddProductPage(this.pageEvent);

  @override
  List<Object> get props => [pageEvent];
}

class GoToCategoriesPage extends PageEvent {
  final PageEvent pageEvent;

  GoToCategoriesPage(this.pageEvent);

  @override
  List<Object> get props => [pageEvent];
}

class GoToSubcategoriesPage extends PageEvent {
  final PageEvent pageEvent;
  final int categoriesIndex;

  GoToSubcategoriesPage(this.pageEvent, this.categoriesIndex);

  @override
  List<Object> get props => [pageEvent, categoriesIndex];
}

class GoToVariasiPage extends PageEvent {
  final PageEvent pageEvent;

  GoToVariasiPage(this.pageEvent);

  @override
  List<Object> get props => [pageEvent];
}

class GoToGrosirPage extends PageEvent {
  final PageEvent pageEvent;

  GoToGrosirPage(this.pageEvent);

  @override
  List<Object> get props => [pageEvent];
}

class GoToOngkisKirimPage extends PageEvent {
  final PageEvent pageEvent;

  GoToOngkisKirimPage(this.pageEvent);

  @override
  List<Object> get props => [pageEvent];
}

class GoToEtalasePage extends PageEvent {
  final PageEvent pageEvent;

  GoToEtalasePage(this.pageEvent);

  @override
  List<Object> get props => [pageEvent];
}

class GoToSaldoPage extends PageEvent {
  final PageEvent pageEvent;

  GoToSaldoPage(this.pageEvent);

  @override
  List<Object> get props => [pageEvent];
}

class GoToDeliveryServicePage extends PageEvent {
  final PageEvent pageEvent;

  GoToDeliveryServicePage(this.pageEvent);

  @override
  List<Object> get props => [pageEvent];
}

class GoToStoreAppraisalPage extends PageEvent {
  final PageEvent pageEvent;

  GoToStoreAppraisalPage(this.pageEvent);

  @override
  List<Object> get props => [pageEvent];
}

class GoToBusinessPage extends PageEvent {
  final PageEvent pageEvent;

  GoToBusinessPage(this.pageEvent);

  @override
  List<Object> get props => [pageEvent];
}

class GoToOrderPage extends PageEvent {
  final PageEvent pageEvent;

  GoToOrderPage(this.pageEvent);

  @override
  List<Object> get props => [pageEvent];
}

class GoToSendPage extends PageEvent {
  final PageEvent pageEvent;

  GoToSendPage(this.pageEvent);

  @override
  List<Object> get props => [pageEvent];
}

class GoToComplainPage extends PageEvent {
  final PageEvent pageEvent;

  GoToComplainPage(this.pageEvent);

  @override
  List<Object> get props => [pageEvent];
}

class GoToDiscussPage extends PageEvent {
  final PageEvent pageEvent;

  GoToDiscussPage(this.pageEvent);

  @override
  List<Object> get props => [pageEvent];
}

class GoToProductPage extends PageEvent {
  final PageEvent pageEvent;

  GoToProductPage(this.pageEvent);

  @override
  List<Object> get props => [pageEvent];
}

class GoToPromoPage extends PageEvent {
  final PageEvent pageEvent;

  GoToPromoPage(this.pageEvent);

  @override
  List<Object> get props => [pageEvent];
}

class GoToAddPromoPage extends PageEvent {
  final PageEvent pageEvent;

  GoToAddPromoPage(this.pageEvent);

  @override
  List<Object> get props => [pageEvent];
}

class GoToLoanPage extends PageEvent {
  final PageEvent pageEvent;

  GoToLoanPage(this.pageEvent);

  @override
  List<Object> get props => [pageEvent];
}

class GoToLoanaPage extends PageEvent {
  final PageEvent pageEvent;

  GoToLoanaPage(this.pageEvent);

  @override
  List<Object> get props => [pageEvent];
}

class GoToLoanbPage extends PageEvent {
  final PageEvent pageEvent;

  GoToLoanbPage(this.pageEvent);

  @override
  List<Object> get props => [pageEvent];
}

class GoToLoancPage extends PageEvent {
  final PageEvent pageEvent;

  GoToLoancPage(this.pageEvent);

  @override
  List<Object> get props => [pageEvent];
}

class GoToLoandPage extends PageEvent {
  final PageEvent pageEvent;

  GoToLoandPage(this.pageEvent);

  @override
  List<Object> get props => [pageEvent];
}

class GoToLoanePage extends PageEvent {
  final PageEvent pageEvent;

  GoToLoanePage(this.pageEvent);

  @override
  List<Object> get props => [pageEvent];
}

class GoToLoanfPage extends PageEvent {
  final PageEvent pageEvent;

  GoToLoanfPage(this.pageEvent);

  @override
  List<Object> get props => [pageEvent];
}

class GoToLoangPage extends PageEvent {
  final PageEvent pageEvent;

  GoToLoangPage(this.pageEvent);

  @override
  List<Object> get props => [pageEvent];
}

class GoToLoanhPage extends PageEvent {
  final PageEvent pageEvent;

  GoToLoanhPage(this.pageEvent);

  @override
  List<Object> get props => [pageEvent];
}

class GoToLoaniPage extends PageEvent {
  final PageEvent pageEvent;

  GoToLoaniPage(this.pageEvent);

  @override
  List<Object> get props => [pageEvent];
}

class GoToLoanjPage extends PageEvent {
  final PageEvent pageEvent;

  GoToLoanjPage(this.pageEvent);

  @override
  List<Object> get props => [pageEvent];
}

class GoToLoankPage extends PageEvent {
  final PageEvent pageEvent;

  GoToLoankPage(this.pageEvent);

  @override
  List<Object> get props => [pageEvent];
}

class GoToLoanlPage extends PageEvent {
  final PageEvent pageEvent;

  GoToLoanlPage(this.pageEvent);

  @override
  List<Object> get props => [pageEvent];
}

class GoToLoanResultPage extends PageEvent {
  final PageEvent pageEvent;

  GoToLoanResultPage(this.pageEvent);

  @override
  List<Object> get props => [pageEvent];
}
