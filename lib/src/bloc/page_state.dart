part of 'page_bloc.dart';

abstract class PageState extends Equatable {
  const PageState();
}

class OnInitialPage extends PageState {
  @override
  List<Object> get props => [];
}

class OnSplashPage extends PageState {
  @override
  List<Object> get props => [];
}

class OnSignInPage extends PageState {
  @override
  List<Object> get props => [];
}

class OnMainPage extends PageState {
  final int bottomNavBarIndex;

  OnMainPage({this.bottomNavBarIndex = 0});

  @override
  List<Object> get props => [bottomNavBarIndex];
}

class OnChatPage extends PageState {
  final PageEvent pageEvent;

  OnChatPage(this.pageEvent);

  @override
  List<Object> get props => [];
}

class OnEditProfilePage extends PageState {
  final PageEvent pageEvent;

  OnEditProfilePage(this.pageEvent);

  @override
  List<Object> get props => [pageEvent];
}

class OnChangePasswordPage extends PageState {
  final PageEvent pageEvent;

  OnChangePasswordPage(this.pageEvent);

  @override
  List<Object> get props => [pageEvent];
}

class OnAddProductPage extends PageState {
  final PageEvent pageEvent;

  OnAddProductPage(this.pageEvent);

  @override
  List<Object> get props => [pageEvent];
}

class OnCategoriesPage extends PageState {
  final PageEvent pageEvent;

  OnCategoriesPage(this.pageEvent);

  @override
  List<Object> get props => [pageEvent];
}

class OnSubcategoriesPage extends PageState {
  final PageEvent pageEvent;
  final int categoriesIndex;

  OnSubcategoriesPage(this.pageEvent, this.categoriesIndex);

  @override
  List<Object> get props => [pageEvent, categoriesIndex];
}

class OnVariasiPage extends PageState {
  final PageEvent pageEvent;

  OnVariasiPage(this.pageEvent);

  @override
  List<Object> get props => [pageEvent];
}

class OnGrosirPage extends PageState {
  final PageEvent pageEvent;

  OnGrosirPage(this.pageEvent);

  @override
  List<Object> get props => [pageEvent];
}

class OnOngkosKirimPage extends PageState {
  final PageEvent pageEvent;

  OnOngkosKirimPage(this.pageEvent);

  @override
  List<Object> get props => [pageEvent];
}

class OnEtalasePage extends PageState {
  final PageEvent pageEvent;

  OnEtalasePage(this.pageEvent);

  @override
  List<Object> get props => [pageEvent];
}

class OnSaldoPage extends PageState {
  final PageEvent pageEvent;

  OnSaldoPage(this.pageEvent);

  @override
  List<Object> get props => [pageEvent];
}

class OnDeliveryServicePage extends PageState {
  final PageEvent pageEvent;

  OnDeliveryServicePage(this.pageEvent);

  @override
  List<Object> get props => [pageEvent];
}

class OnStoreAppraisalPage extends PageState {
  final PageEvent pageEvent;

  OnStoreAppraisalPage(this.pageEvent);

  @override
  List<Object> get props => [pageEvent];
}

class OnBusinessPage extends PageState {
  final PageEvent pageEvent;

  OnBusinessPage(this.pageEvent);

  @override
  List<Object> get props => [pageEvent];
}

class OnOrderPage extends PageState {
  final PageEvent pageEvent;

  OnOrderPage(this.pageEvent);

  @override
  List<Object> get props => [pageEvent];
}

class OnSendPage extends PageState {
  final PageEvent pageEvent;

  OnSendPage(this.pageEvent);

  @override
  List<Object> get props => [pageEvent];
}

class OnComplainPage extends PageState {
  final PageEvent pageEvent;

  OnComplainPage(this.pageEvent);

  @override
  List<Object> get props => [pageEvent];
}

class OnDiscussPage extends PageState {
  final PageEvent pageEvent;

  OnDiscussPage(this.pageEvent);

  @override
  List<Object> get props => [pageEvent];
}

class OnProductPage extends PageState {
  final PageEvent pageEvent;

  OnProductPage(this.pageEvent);

  @override
  List<Object> get props => [pageEvent];
}

class OnPromoPage extends PageState {
  final PageEvent pageEvent;

  OnPromoPage(this.pageEvent);

  @override
  List<Object> get props => [pageEvent];
}

class OnAddPromoPage extends PageState {
  final PageEvent pageEvent;

  OnAddPromoPage(this.pageEvent);

  @override
  List<Object> get props => [pageEvent];
}

class OnLoanPage extends PageState {
  final PageEvent pageEvent;

  OnLoanPage(this.pageEvent);

  @override
  List<Object> get props => [pageEvent];
}

class OnLoanaPage extends PageState {
  final PageEvent pageEvent;

  OnLoanaPage(this.pageEvent);

  @override
  List<Object> get props => [pageEvent];
}

class OnLoanbPage extends PageState {
  final PageEvent pageEvent;

  OnLoanbPage(this.pageEvent);

  @override
  List<Object> get props => [pageEvent];
}

class OnLoancPage extends PageState {
  final PageEvent pageEvent;

  OnLoancPage(this.pageEvent);

  @override
  List<Object> get props => [pageEvent];
}

class OnLoandPage extends PageState {
  final PageEvent pageEvent;

  OnLoandPage(this.pageEvent);

  @override
  List<Object> get props => [pageEvent];
}

class OnLoanePage extends PageState {
  final PageEvent pageEvent;

  OnLoanePage(this.pageEvent);

  @override
  List<Object> get props => [pageEvent];
}

class OnLoanfPage extends PageState {
  final PageEvent pageEvent;

  OnLoanfPage(this.pageEvent);

  @override
  List<Object> get props => [pageEvent];
}

class OnLoangPage extends PageState {
  final PageEvent pageEvent;

  OnLoangPage(this.pageEvent);

  @override
  List<Object> get props => [pageEvent];
}

class OnLoanhPage extends PageState {
  final PageEvent pageEvent;

  OnLoanhPage(this.pageEvent);

  @override
  List<Object> get props => [pageEvent];
}

class OnLoaniPage extends PageState {
  final PageEvent pageEvent;

  OnLoaniPage(this.pageEvent);

  @override
  List<Object> get props => [pageEvent];
}

class OnLoanjPage extends PageState {
  final PageEvent pageEvent;

  OnLoanjPage(this.pageEvent);

  @override
  List<Object> get props => [pageEvent];
}

class OnLoankPage extends PageState {
  final PageEvent pageEvent;

  OnLoankPage(this.pageEvent);

  @override
  List<Object> get props => [pageEvent];
}

class OnLoanlPage extends PageState {
  final PageEvent pageEvent;

  OnLoanlPage(this.pageEvent);

  @override
  List<Object> get props => [pageEvent];
}

class OnLoanResultPage extends PageState {
  final PageEvent pageEvent;

  OnLoanResultPage(this.pageEvent);

  @override
  List<Object> get props => [pageEvent];
}
