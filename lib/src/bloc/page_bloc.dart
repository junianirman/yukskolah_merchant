import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

part 'page_event.dart';
part 'page_state.dart';

class PageBloc extends Bloc<PageEvent, PageState> {
  PageBloc() : super(OnInitialPage());

  @override
  Stream<PageState> mapEventToState(
    PageEvent event,
  ) async* {
    if (event is GoToSplashPage) {
      yield OnSplashPage();
    } else if (event is GoToSignInPage) {
      yield OnSignInPage();
    } else if (event is GoToMainPage) {
      yield OnMainPage(bottomNavBarIndex: event.bottomNavBarIndex);
    } else if (event is GoToChatPage) {
      yield OnChatPage(event.pageEvent);
    } else if (event is GoToEditProfilePage) {
      yield OnEditProfilePage(event.pageEvent);
    } else if (event is GoToChangePasswordPage) {
      yield OnChangePasswordPage(event.pageEvent);
    } else if (event is GoToAddProductPage) {
      yield OnAddProductPage(event.pageEvent);
    } else if (event is GoToCategoriesPage) {
      yield OnCategoriesPage(event.pageEvent);
    } else if (event is GoToVariasiPage) {
      yield OnVariasiPage(event.pageEvent);
    } else if (event is GoToGrosirPage) {
      yield OnGrosirPage(event.pageEvent);
    } else if (event is GoToOngkisKirimPage) {
      yield OnOngkosKirimPage(event.pageEvent);
    } else if (event is GoToEtalasePage) {
      yield OnEtalasePage(event.pageEvent);
    } else if (event is GoToSubcategoriesPage) {
      yield OnSubcategoriesPage(event.pageEvent, event.categoriesIndex);
    } else if (event is GoToSaldoPage) {
      yield OnSaldoPage(event.pageEvent);
    } else if (event is GoToDeliveryServicePage) {
      yield OnDeliveryServicePage(event.pageEvent);
    } else if (event is GoToStoreAppraisalPage) {
      yield OnStoreAppraisalPage(event.pageEvent);
    } else if (event is GoToBusinessPage) {
      yield OnBusinessPage(event.pageEvent);
    } else if (event is GoToOrderPage) {
      yield OnOrderPage(event.pageEvent);
    } else if (event is GoToSendPage) {
      yield OnSendPage(event.pageEvent);
    } else if (event is GoToComplainPage) {
      yield OnComplainPage(event.pageEvent);
    } else if (event is GoToDiscussPage) {
      yield OnDiscussPage(event.pageEvent);
    } else if (event is GoToProductPage) {
      yield OnProductPage(event.pageEvent);
    } else if (event is GoToPromoPage) {
      yield OnPromoPage(event.pageEvent);
    } else if (event is GoToAddPromoPage) {
      yield OnAddPromoPage(event.pageEvent);
    } else if (event is GoToLoanPage) {
      yield OnLoanPage(event.pageEvent);
    } else if (event is GoToLoanaPage) {
      yield OnLoanaPage(event.pageEvent);
    } else if (event is GoToLoanbPage) {
      yield OnLoanbPage(event.pageEvent);
    } else if (event is GoToLoancPage) {
      yield OnLoancPage(event.pageEvent);
    } else if (event is GoToLoandPage) {
      yield OnLoandPage(event.pageEvent);
    } else if (event is GoToLoanePage) {
      yield OnLoanePage(event.pageEvent);
    } else if (event is GoToLoanfPage) {
      yield OnLoanfPage(event.pageEvent);
    } else if (event is GoToLoangPage) {
      yield OnLoangPage(event.pageEvent);
    } else if (event is GoToLoanhPage) {
      yield OnLoanhPage(event.pageEvent);
    } else if (event is GoToLoaniPage) {
      yield OnLoaniPage(event.pageEvent);
    } else if (event is GoToLoanjPage) {
      yield OnLoanjPage(event.pageEvent);
    } else if (event is GoToLoankPage) {
      yield OnLoankPage(event.pageEvent);
    } else if (event is GoToLoanlPage) {
      yield OnLoanlPage(event.pageEvent);
    } else if (event is GoToLoanResultPage) {
      yield OnLoanResultPage(event.pageEvent);
    }
  }
}
