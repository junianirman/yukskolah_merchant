part of 'widgets.dart';

class BusinessCard extends StatelessWidget {
  final String title;
  final int sumBusiness;
  final int presentase;

  const BusinessCard(this.title, this.sumBusiness, this.presentase, {Key key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 100,
      width: MediaQuery.of(context).size.width / 3.2 - 8,
      padding: EdgeInsets.all(8),
      decoration: BoxDecoration(
        color: greyColor,
        borderRadius: BorderRadius.circular(8),
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(title, style: blackTextFont),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              title == "Penjualan" || title == "Penjualan Perpesanan"
                  ? Text(
                      NumberFormat.currency(
                              locale: "id_ID", decimalDigits: 0, symbol: "Rp ")
                          .format(sumBusiness),
                      style:
                          blackTextFont.copyWith(fontWeight: FontWeight.w400),
                    )
                  : Text(
                      "$sumBusiness",
                      style: blackTextFont,
                    ),
              SizedBox(height: 4),
              Text(
                "$presentase %",
                style: blackTextFont,
              )
            ],
          ),
        ],
      ),
    );
  }
}
