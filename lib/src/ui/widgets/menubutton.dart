part of 'widgets.dart';

class MenuButton extends StatelessWidget {
  final String title;
  final Function onTap;

  const MenuButton(this.title, {this.onTap, Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        if (onTap != null) {
          onTap();
        }
      },
      child: Column(
        children: <Widget>[
          Container(
            margin: EdgeInsets.only(bottom: 4),
            width: 100,
            // width:
            //     (MediaQuery.of(context).size.width - 2 * defaultMargin - 24) / 2,
            height: 100,
            child: Center(
              child: SizedBox(
                  height: 114,
                  child: Image(image: AssetImage(getImageFromGenre(title)))),
            ),
          ),
          Text(
            title,
            style: blackTextFont.copyWith(fontSize: 14),
          )
        ],
      ),
    );
  }

  String getImageFromGenre(String title) {
    switch (title) {
      case "Order":
        return "assets/ic_order.png";
        break;
      case "Send":
        return "assets/ic_send.png";
        break;
      case "Complain":
        return "assets/ic_complain.png";
        break;
      case "Discuss":
        return "assets/ic_discuss.png";
        break;
      case "Produk":
        return "assets/ic_product.png";
        break;
      case "Promo":
        return "assets/ic_promo.png";
        break;
      case "Loan":
        return "assets/ic_loan.png";
        break;
      default:
        return "";
    }
  }
}
