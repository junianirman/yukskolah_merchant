part of 'widgets.dart';

class CustomListTile extends StatelessWidget {
  final String title;
  final Function onTap;

  const CustomListTile(this.title, {this.onTap, Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 40,
      child: ListTile(
        leading: Text(title, style: blackTextFont),
        trailing: Icon(Icons.arrow_forward_ios),
        onTap: () {
          if (onTap != null) {
            onTap();
          }
        },
      ),
    );
  }
}
