part of 'widgets.dart';

class CustomListTileIcon extends StatelessWidget {
  final String title;
  final Function onTap;

  const CustomListTileIcon(this.title, {this.onTap, Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 36,
      child: ListTile(
        leading: Icon(Icons.list_alt),
        title: Transform.translate(
          offset: Offset(-16, 0),
          child: Text(title, style: blackTextFont),
        ),
        trailing: Icon(Icons.arrow_forward_ios),
        contentPadding: EdgeInsets.symmetric(horizontal: 0),
        onTap: () {
          if (onTap != null) {
            onTap();
          }
        },
      ),
    );
  }
}
