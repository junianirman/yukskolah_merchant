part of 'widgets.dart';

class CustomTabbarLanguage extends StatelessWidget {
  final int selectedIndex;
  final List<String> titles;
  final Function(int) onTap;

  const CustomTabbarLanguage(
      {@required this.titles, this.selectedIndex, this.onTap, Key key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 40,
      child: Stack(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: titles
                .map(
                  (e) => GestureDetector(
                    onTap: () {
                      if (onTap != null) {
                        onTap(titles.indexOf(e));
                      }
                    },
                    child: Container(
                      width: MediaQuery.of(context).size.width / 3.2,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(8),
                        color: (titles.indexOf(e) == selectedIndex)
                            ? accentColor2
                            : Color(0xFFFFFFFF),
                      ),
                      child: Center(
                        child: Text(
                          e,
                          style: (titles.indexOf(e) == selectedIndex)
                              ? linkTextFont.copyWith(
                                  fontStyle: FontStyle.normal,
                                  decoration: TextDecoration.none)
                              : greyTextFont,
                        ),
                      ),
                    ),
                  ),
                )
                .toList(),
          )
        ],
      ),
    );
  }
}
