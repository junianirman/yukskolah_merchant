part of 'widgets.dart';

class SalesCard extends StatelessWidget {
  final String title;
  final int total;

  const SalesCard(this.title, this.total, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 50,
      padding: EdgeInsets.symmetric(vertical: 8, horizontal: defaultMargin),
      margin: EdgeInsets.only(bottom: defaultMargin),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(8),
          border: Border.all(color: mainColor)),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            title,
            style: blackTextFont,
          ),
          Text(
            total.toString(),
            style: blackTextFont,
          )
        ],
      ),
    );
  }
}
