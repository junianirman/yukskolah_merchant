part of 'widgets.dart';

class DikemasCard extends StatelessWidget {
  final Transaction transaction;

  const DikemasCard(this.transaction, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(8),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Row(
                children: [
                  Container(
                    height: 30,
                    width: 30,
                    decoration: BoxDecoration(
                      color: greyColor,
                      borderRadius: BorderRadius.circular(4),
                      image: DecorationImage(
                        image: AssetImage(transaction.buyer.picturePath),
                      ),
                    ),
                  ),
                  SizedBox(width: 8),
                  Container(
                    width: MediaQuery.of(context).size.width / 1.6 - 8,
                    child: Text(
                      transaction.buyer.name,
                      style: blackTextFont,
                      overflow: TextOverflow.clip,
                      maxLines: 1,
                    ),
                  ),
                ],
              ),
              Text(
                transaction.status,
                style: blackTextFont.copyWith(color: redColor),
              ),
            ],
          ),
          SizedBox(height: 8),
          Row(
            children: [
              Container(
                height: 100,
                width: 100,
                decoration: BoxDecoration(
                  color: greyColor,
                  borderRadius: BorderRadius.circular(4),
                  image: DecorationImage(
                    image: AssetImage(transaction.product.picturePath),
                  ),
                ),
              ),
              SizedBox(width: 8),
              Container(
                height: 100,
                width: MediaQuery.of(context).size.width / 1.6 + 6,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Column(
                      children: [
                        Row(
                          children: [
                            Container(
                              width: MediaQuery.of(context).size.width / 1.6,
                              child: Text(
                                transaction.product.name,
                                style: blackTextFont,
                                overflow: TextOverflow.clip,
                                maxLines: 2,
                              ),
                            ),
                          ],
                        ),
                        SizedBox(height: 4),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              "Warna ${transaction.product.color[3]}, Size ${transaction.product.size[0]}",
                              style: blackTextFont,
                            ),
                            Text(
                              "x ${transaction.quantity}",
                              style: blackTextFont,
                            ),
                          ],
                        ),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        Text(
                          NumberFormat.currency(
                                  locale: "id_ID",
                                  decimalDigits: 0,
                                  symbol: "Rp ")
                              .format(transaction.product.price),
                          style: blackTextFont.copyWith(
                              fontWeight: FontWeight.w400),
                        ),
                      ],
                    )
                  ],
                ),
              ),
            ],
          ),
          SizedBox(height: 8),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text("${transaction.quantity} Produk", style: blackTextFont),
              Text(
                "Total Pesanan : " +
                    NumberFormat.currency(
                            locale: "id_ID", decimalDigits: 0, symbol: "Rp ")
                        .format(transaction.total),
                style: blackTextFont.copyWith(fontWeight: FontWeight.w400),
              ),
            ],
          ),
          SizedBox(height: 8),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text("No. Pesanan", style: blackTextFont),
              Text(transaction.orderNumber, style: blackTextFont),
            ],
          ),
          SizedBox(height: defaultMargin),
          Container(
            width: MediaQuery.of(context).size.width,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Container(
                  width: MediaQuery.of(context).size.width / 3.2,
                  height: 45,
                  child: RaisedButton(
                    onPressed: () {},
                    elevation: 0,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(8)),
                    color: darkGreyColor,
                    child: Text(
                      'Proses',
                      style: blackTextFont,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
