part of 'widgets.dart';

class CustomTabbarProduct extends StatelessWidget {
  final List<ProductTabMenu> productTabMenu;
  final int selectedIndex;
  final Function(int) onTap;

  const CustomTabbarProduct(
      {this.productTabMenu, this.selectedIndex, this.onTap, Key key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 36,
      margin: EdgeInsets.all(4),
      child: Stack(
        children: [
          Container(
            margin: EdgeInsets.only(top: 45),
            height: 1,
            color: Color(0xFFF2F2F2),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: productTabMenu
                .map(
                  (e) => GestureDetector(
                    onTap: () {
                      if (onTap != null) {
                        onTap(productTabMenu.indexOf(e));
                      }
                    },
                    child: Column(
                      children: [
                        Text(
                          e.title,
                          style: (productTabMenu.indexOf(e) == selectedIndex)
                              ? blackTextFont.copyWith(
                                  fontWeight: FontWeight.w500)
                              : greyTextFont,
                        ),
                        SizedBox(height: 4),
                        Text(
                          "(${e.sumProduct})",
                          style: (productTabMenu.indexOf(e) == selectedIndex)
                              ? blackTextFont.copyWith(
                                  fontWeight: FontWeight.w500)
                              : greyTextFont,
                        ),
                      ],
                    ),
                  ),
                )
                .toList(),
          ),
        ],
      ),
    );
  }
}
