part of 'pages.dart';

class ChatPage extends StatefulWidget {
  final PageEvent pageEvent;

  ChatPage(this.pageEvent, {Key key}) : super(key: key);

  @override
  _ChatPageState createState() => _ChatPageState();
}

class _ChatPageState extends State<ChatPage> {
  TextEditingController textChatController = TextEditingController();

  List<String> _listChat = [];

  addChat(val) {
    setState(() {
      _listChat.add(val);
    });
  }

  @override
  Widget build(BuildContext context) {
    BlocProvider.of<ThemeBloc>(context)
        .add(ChangeTheme(ThemeData().copyWith(primaryColor: mainColor)));

    return WillPopScope(
      onWillPop: () {
        context.read<PageBloc>().add(widget.pageEvent);
        return;
      },
      child: GeneralPage(
        title: "Chat",
        onBackButtonPressed: () {
          context.read<PageBloc>().add(widget.pageEvent);
        },
        child: Column(
          children: [
            listContent(),
            Align(
              alignment: Alignment.bottomCenter,
              child: Container(
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(12),
                        topRight: Radius.circular(12)),
                    border: Border.all(color: mainColor)),
                child: TextField(
                  controller: textChatController,
                  onSubmitted: (value) {
                    addChat(value);
                  },
                  decoration: InputDecoration(
                      hintText: "Type here",
                      contentPadding: EdgeInsets.symmetric(
                          vertical: 8, horizontal: defaultMargin),
                      border: InputBorder.none),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget listContent() {
    return Expanded(
      child: ListView.builder(
        scrollDirection: Axis.vertical,
        itemCount: _listChat.length,
        itemBuilder: (context, index) {
          if (index < _listChat.length) {
            return buildAnswerItem(_listChat[index]);
          }
          return null;
        },
      ),
    );
  }

  Widget buildAnswerItem(val) {
    return ListTile(
      title: Text(
        val,
        textAlign: TextAlign.end,
      ),
    );
  }
}
