part of 'pages.dart';

class LoanhPage extends StatefulWidget {
  final PageEvent pageEvent;

  LoanhPage(this.pageEvent, {Key key}) : super(key: key);

  @override
  _LoanhPageState createState() => _LoanhPageState();
}

class _LoanhPageState extends State<LoanhPage> {
  TextEditingController amountController = TextEditingController();
  int selectedAmount = 0;
  Npwp npwp = Npwp.yes;

  @override
  Widget build(BuildContext context) {
    BlocProvider.of<ThemeBloc>(context)
        .add(ChangeTheme(ThemeData().copyWith(primaryColor: mainColor)));

    return WillPopScope(
      onWillPop: () {
        context.read<PageBloc>().add(widget.pageEvent);
        return;
      },
      child: GeneralPage(
        title: "Loan",
        onBackButtonPressed: () {
          context.read<PageBloc>().add(GoToMainPage());
        },
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              margin: EdgeInsets.symmetric(horizontal: defaultMargin),
              child: Text("Apakah anda memiliki kartu NPWP ?",
                  style: blackTextFont.copyWith(fontSize: 16)),
            ),
            Container(
              height: 50,
              width: double.infinity,
              margin: EdgeInsets.fromLTRB(
                  defaultMargin, 0, defaultMargin, defaultMargin),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    height: 32,
                    width: 130,
                    child: RadioListTile(
                      title: Text("Ya", style: blackTextFont),
                      activeColor: greenColor,
                      value: Npwp.yes,
                      groupValue: npwp,
                      onChanged: (Npwp value) {
                        setState(() {
                          npwp = value;
                        });
                      },
                    ),
                  ),
                  Container(
                    height: 32,
                    width: 130,
                    child: RadioListTile(
                      title: Text("Tidak", style: blackTextFont),
                      activeColor: greenColor,
                      value: Npwp.no,
                      groupValue: npwp,
                      onChanged: (Npwp value) {
                        setState(() {
                          npwp = value;
                        });
                      },
                    ),
                  ),
                ],
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width / 1.4,
              margin: EdgeInsets.only(top: defaultMargin),
              height: 45,
              padding: EdgeInsets.symmetric(horizontal: defaultMargin),
              child: RaisedButton(
                onPressed: () {
                  context.read<PageBloc>().add(GoToLoaniPage(widget.pageEvent));
                },
                elevation: 0,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(8)),
                color: orangeColor,
                child: Text(
                  'Next',
                  style: blackTextFont.copyWith(fontSize: 16),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
