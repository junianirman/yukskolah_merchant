part of 'pages.dart';

class ProductPage extends StatefulWidget {
  final PageEvent pageEvent;

  const ProductPage(this.pageEvent, {Key key}) : super(key: key);

  @override
  _ProductPageState createState() => _ProductPageState();
}

class _ProductPageState extends State<ProductPage> {
  int selectedIndex = 0;

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        context.read<PageBloc>().add(widget.pageEvent);
        return;
      },
      child: GeneralPage(
        title: "Produk Saya",
        onBackButtonPressed: () {
          context.read<PageBloc>().add(widget.pageEvent);
        },
        child: Stack(
          children: [
            Column(
              children: [
                Container(
                  margin: EdgeInsets.symmetric(horizontal: defaultMargin),
                  child: CustomTabbarProduct(
                    productTabMenu: dummyProductTabMenu,
                    selectedIndex: selectedIndex,
                    onTap: (index) {
                      setState(() {
                        selectedIndex = index;
                      });
                    },
                  ),
                ),
                Container(
                  margin: EdgeInsets.symmetric(horizontal: defaultMargin),
                  height: 48,
                  width: double.infinity,
                  child: Column(
                    children: [
                      Divider(color: Colors.black, thickness: 1),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          Text(
                            "Baru",
                            style: blackTextFont,
                          ),
                          Row(
                            children: [
                              Text(
                                "Stok",
                                style: blackTextFont,
                              ),
                              Icon(Icons.sort, size: 14),
                            ],
                          ),
                        ],
                      ),
                      Divider(color: Colors.black, thickness: 1)
                    ],
                  ),
                ),
                Expanded(
                  child: ListView(
                    children: dummyProduct
                        .map(
                          (e) => selectedIndex == 0 &&
                                  e.productTab == ProductTab.live
                              ? Container(
                                  margin: EdgeInsets.fromLTRB(
                                    defaultMargin,
                                    defaultMargin,
                                    defaultMargin,
                                    (dummyProduct.indexOf(e) ==
                                            dummyProduct.length - 1
                                        ? defaultMargin
                                        : 0),
                                  ),
                                  height:
                                      MediaQuery.of(context).size.height / 4.8 +
                                          14,
                                  decoration: BoxDecoration(
                                    color: greyColor,
                                    borderRadius: BorderRadius.circular(8),
                                  ),
                                  child: ProductCard(e),
                                )
                              : selectedIndex == 1 &&
                                      e.productTab == ProductTab.habis
                                  ? Container(
                                      margin: EdgeInsets.fromLTRB(
                                        defaultMargin,
                                        defaultMargin,
                                        defaultMargin,
                                        (dummyProduct.indexOf(e) ==
                                                dummyProduct.length - 1
                                            ? defaultMargin
                                            : 0),
                                      ),
                                      height:
                                          MediaQuery.of(context).size.height /
                                                  4.8 +
                                              14,
                                      decoration: BoxDecoration(
                                        color: greyColor,
                                        borderRadius: BorderRadius.circular(8),
                                      ),
                                      child: ProductCard(e),
                                    )
                                  : selectedIndex == 2 &&
                                          e.productTab == ProductTab.diblokir
                                      ? Container(
                                          margin: EdgeInsets.fromLTRB(
                                            defaultMargin,
                                            defaultMargin,
                                            defaultMargin,
                                            (dummyProduct.indexOf(e) ==
                                                    dummyProduct.length - 1
                                                ? defaultMargin
                                                : 0),
                                          ),
                                          height: MediaQuery.of(context)
                                                      .size
                                                      .height /
                                                  4.8 +
                                              14,
                                          decoration: BoxDecoration(
                                            color: greyColor,
                                            borderRadius:
                                                BorderRadius.circular(8),
                                          ),
                                          child: ProductCard(e),
                                        )
                                      : selectedIndex == 3 &&
                                              e.productTab ==
                                                  ProductTab.diarsipkan
                                          ? Container(
                                              margin: EdgeInsets.fromLTRB(
                                                defaultMargin,
                                                defaultMargin,
                                                defaultMargin,
                                                (dummyProduct.indexOf(e) ==
                                                        dummyProduct.length - 1
                                                    ? defaultMargin
                                                    : 0),
                                              ),
                                              height: MediaQuery.of(context)
                                                          .size
                                                          .height /
                                                      4.8 +
                                                  14,
                                              decoration: BoxDecoration(
                                                color: greyColor,
                                                borderRadius:
                                                    BorderRadius.circular(8),
                                              ),
                                              child: ProductCard(e),
                                            )
                                          : Container(),
                        )
                        .toList(),
                  ),
                ),
              ],
            ),
            Align(
              alignment: Alignment.bottomRight,
              child: Container(
                margin: EdgeInsets.all(defaultMargin),
                child: FloatingActionButton(
                  onPressed: () {
                    context.read<PageBloc>().add(
                        GoToAddProductPage(GoToProductPage(widget.pageEvent)));
                  },
                  child: Container(
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      image: DecorationImage(
                        image: AssetImage('assets/ic_plus.png'),
                      ),
                    ),
                  ),
                  elevation: 0,
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
