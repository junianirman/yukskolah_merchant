part of 'pages.dart';

class BusinessPage extends StatelessWidget {
  final PageEvent pageEvent;

  const BusinessPage(this.pageEvent, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        context.read<PageBloc>().add(pageEvent);
        return;
      },
      child: GeneralPage(
        title: "Penilaian Toko",
        onBackButtonPressed: () {
          context.read<PageBloc>().add(pageEvent);
        },
        child: ListView(
          children: [
            Container(
              margin: EdgeInsets.all(defaultMargin),
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text("Real Time", style: blackTextFont),
                      Text("Kemarin", style: blackTextFont),
                      Text("7 Hari", style: blackTextFont),
                      Text("30 Hari", style: blackTextFont),
                    ],
                  ),
                  SizedBox(height: 8),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text("Kriteria Utama", style: blackTextFont),
                      Text("17 des - 23 des", style: blackTextFont),
                    ],
                  ),
                  SizedBox(height: 8),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      BusinessCard("Pesanan", dummyBusiness.pesanan,
                          dummyBusiness.presentasePesanan),
                      BusinessCard("Penjualan", dummyBusiness.penjualan,
                          dummyBusiness.presentasePenjualan),
                      BusinessCard(
                          "Tingkat Konversi",
                          dummyBusiness.tingkatKonversi,
                          dummyBusiness.presentaseTingkatKonversi),
                    ],
                  ),
                  SizedBox(height: 8),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      BusinessCard(
                          "Penjualan Perpesanan",
                          dummyBusiness.penjualanPerpesanan,
                          dummyBusiness.presentasePenjualanPerpesanan),
                      BusinessCard(
                          "Total Pengunjung",
                          dummyBusiness.totalPengunjung,
                          dummyBusiness.presentaseTotalPengunjung),
                      BusinessCard(
                          "Produk di Lihat",
                          dummyBusiness.produkDilihat,
                          dummyBusiness.presentaseProdukDilihat),
                    ],
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
