part of 'pages.dart';

class Wrapper extends StatelessWidget {
  const Wrapper({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (!(prevPageEvent is GoToSplashPage)) {
      prevPageEvent = GoToSplashPage();
      context.watch<PageBloc>().add(prevPageEvent);
    }

    return BlocBuilder<PageBloc, PageState>(
        builder: (_, pageState) => (pageState is OnSplashPage)
            ? SplashPage()
            : (pageState is OnSignInPage)
                ? Signinpage()
                : (pageState is OnMainPage)
                    ? MainPage(bottomNavBarIndex: pageState.bottomNavBarIndex)
                    : (pageState is OnChatPage)
                        ? ChatPage(pageState.pageEvent)
                        : (pageState is OnOrderPage)
                            ? OrderPage(pageState.pageEvent)
                            : (pageState is OnSendPage)
                                ? SendPage(pageState.pageEvent)
                                : (pageState is OnComplainPage)
                                    ? ComplainPage(pageState.pageEvent)
                                    : (pageState is OnDiscussPage)
                                        ? DiscussPage(pageState.pageEvent)
                                        : (pageState is OnProductPage)
                                            ? ProductPage(pageState.pageEvent)
                                            : (pageState is OnPromoPage)
                                                ? PromoPage(pageState.pageEvent)
                                                : (pageState is OnAddPromoPage)
                                                    ? AddPromoPage(
                                                        pageState.pageEvent)
                                                    : (pageState is OnLoanPage)
                                                        ? LoanPage(
                                                            pageState.pageEvent)
                                                        : (pageState
                                                                is OnLoanaPage)
                                                            ? LoanaPage(
                                                                pageState
                                                                    .pageEvent)
                                                            : (pageState
                                                                    is OnLoanbPage)
                                                                ? LoanbPage(
                                                                    pageState
                                                                        .pageEvent)
                                                                : (pageState
                                                                        is OnLoancPage)
                                                                    ? LoancPage(
                                                                        pageState
                                                                            .pageEvent)
                                                                    : (pageState
                                                                            is OnLoandPage)
                                                                        ? LoandPage(pageState
                                                                            .pageEvent)
                                                                        : (pageState
                                                                                is OnLoanePage)
                                                                            ? LoanePage(pageState.pageEvent)
                                                                            : (pageState is OnLoanfPage)
                                                                                ? LoanfPage(pageState.pageEvent)
                                                                                : (pageState is OnLoangPage)
                                                                                    ? LoangPage(pageState.pageEvent)
                                                                                    : (pageState is OnLoanhPage)
                                                                                        ? LoanhPage(pageState.pageEvent)
                                                                                        : (pageState is OnLoaniPage)
                                                                                            ? LoaniPage(pageState.pageEvent)
                                                                                            : (pageState is OnLoanjPage)
                                                                                                ? LoanjPage(pageState.pageEvent)
                                                                                                : (pageState is OnLoankPage)
                                                                                                    ? LoankPage(pageState.pageEvent)
                                                                                                    : (pageState is OnLoanlPage)
                                                                                                        ? LoanlPage(pageState.pageEvent)
                                                                                                        : (pageState is OnAddProductPage)
                                                                                                            ? AddProductPage(pageState.pageEvent)
                                                                                                            : (pageState is OnSaldoPage)
                                                                                                                ? SaldoPage(pageState.pageEvent)
                                                                                                                : (pageState is OnDeliveryServicePage)
                                                                                                                    ? DeliveryServicePage(pageState.pageEvent)
                                                                                                                    : (pageState is OnEditProfilePage)
                                                                                                                        ? EditProfilePage(pageState.pageEvent)
                                                                                                                        : (pageState is OnChangePasswordPage)
                                                                                                                            ? ChangePasswordPage(pageState.pageEvent)
                                                                                                                            : (pageState is OnStoreAppraisalPage)
                                                                                                                                ? StoreAppraisalPage(pageState.pageEvent)
                                                                                                                                : (pageState is OnBusinessPage)
                                                                                                                                    ? BusinessPage(pageState.pageEvent)
                                                                                                                                    : (pageState is OnCategoriesPage)
                                                                                                                                        ? CategoriesPage(pageState.pageEvent)
                                                                                                                                        : (pageState is OnSubcategoriesPage)
                                                                                                                                            ? SubcategoriesPage(pageState.pageEvent, pageState.categoriesIndex)
                                                                                                                                            : (pageState is OnVariasiPage)
                                                                                                                                                ? VariasiPage(pageState.pageEvent)
                                                                                                                                                : (pageState is OnGrosirPage)
                                                                                                                                                    ? GrosirPage(pageState.pageEvent)
                                                                                                                                                    : (pageState is OnOngkosKirimPage)
                                                                                                                                                        ? OngkosKirimPage(pageState.pageEvent)
                                                                                                                                                        : (pageState is OnEtalasePage)
                                                                                                                                                            ? EtalasePage(pageState.pageEvent)
                                                                                                                                                            : Container());
  }
}
