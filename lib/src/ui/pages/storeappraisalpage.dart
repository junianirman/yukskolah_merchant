part of 'pages.dart';

class StoreAppraisalPage extends StatelessWidget {
  final PageEvent pageEvent;

  const StoreAppraisalPage(this.pageEvent, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        context.read<PageBloc>().add(pageEvent);
        return;
      },
      child: GeneralPage(
        title: "Penilaian Toko",
        onBackButtonPressed: () {
          context.read<PageBloc>().add(pageEvent);
        },
        child: ListView(
          children: [
            Container(
              margin: EdgeInsets.all(defaultMargin),
              child: Column(
                children: [
                  Container(
                    height: 200,
                    decoration: BoxDecoration(
                      color: greyColor,
                      borderRadius: BorderRadius.circular(8),
                    ),
                    child: Center(
                      child: Text(
                        "0 Ulasan",
                        style: blackTextFont,
                      ),
                    ),
                  ),
                  SizedBox(height: defaultMargin),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      Column(
                        children: [
                          Text(
                            "Dari Pembeli",
                            style: blackTextFont,
                          ),
                          SizedBox(height: 4),
                          Text(
                            "(0.0 dari 5)",
                            style: blackTextFont,
                          ),
                        ],
                      ),
                      Column(
                        children: [
                          Text(
                            "Dari Penjual",
                            style: blackTextFont,
                          ),
                          SizedBox(height: 4),
                          Text(
                            "(0.0 dari 5)",
                            style: blackTextFont,
                          ),
                        ],
                      ),
                    ],
                  ),
                  SizedBox(height: defaultMargin),
                  Container(
                    height: 200,
                    width: MediaQuery.of(context).size.width,
                    decoration: BoxDecoration(
                      color: greyColor,
                      borderRadius: BorderRadius.circular(8),
                    ),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Container(
                          height: 100,
                          width: 120,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(8),
                            image: DecorationImage(
                                image: AssetImage('assets/thumb.png'),
                                fit: BoxFit.cover),
                          ),
                        ),
                        Text("Belum Ada Penilaian", style: blackTextFont),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
