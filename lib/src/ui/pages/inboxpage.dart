part of 'pages.dart';

class InboxPage extends StatelessWidget {
  const InboxPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double listItemWidth =
        MediaQuery.of(context).size.width - 2 * defaultMargin;

    return SafeArea(
      child: Container(
        width: double.infinity,
        color: Colors.white,
        child: ListView(
          children: dummyInbox
              .map(
                (e) => GestureDetector(
                    onTap: () {
                      context.read<PageBloc>().add(
                          GoToChatPage(GoToMainPage(bottomNavBarIndex: 1)));
                    },
                    child: InboxCard(inbox: e, itemWidth: listItemWidth)),
              )
              .toList(),
        ),
      ),
    );
  }
}
