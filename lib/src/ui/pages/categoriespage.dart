part of 'pages.dart';

class CategoriesPage extends StatelessWidget {
  final PageEvent pageEvent;

  const CategoriesPage(this.pageEvent, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        context.read<PageBloc>().add(pageEvent);
        return;
      },
      child: GeneralPage(
        title: "Kategori",
        onBackButtonPressed: () {
          context.read<PageBloc>().add(pageEvent);
        },
        child: ListView(
          children: [
            CustomListTile(
              "Makanan & Minuman",
              onTap: () {
                context.read<PageBloc>().add(
                    GoToSubcategoriesPage(GoToCategoriesPage(pageEvent), 1));
              },
            ),
            CustomListTile(
              "Fashion Muslim",
              onTap: () {
                context.read<PageBloc>().add(
                    GoToSubcategoriesPage(GoToCategoriesPage(pageEvent), 2));
              },
            ),
            CustomListTile(
              "Pakaian Pria",
              onTap: () {
                context.read<PageBloc>().add(
                    GoToSubcategoriesPage(GoToCategoriesPage(pageEvent), 3));
              },
            ),
            CustomListTile(
              "Pakaian Wanita",
              onTap: () {
                context.read<PageBloc>().add(
                    GoToSubcategoriesPage(GoToCategoriesPage(pageEvent), 4));
              },
            ),
            CustomListTile(
              "Fashion Anak",
              onTap: () {
                context.read<PageBloc>().add(
                    GoToSubcategoriesPage(GoToCategoriesPage(pageEvent), 5));
              },
            ),
            CustomListTile(
              "Komputer & Aksesoris",
              onTap: () {
                context.read<PageBloc>().add(
                    GoToSubcategoriesPage(GoToCategoriesPage(pageEvent), 6));
              },
            ),
            CustomListTile(
              "Olahraga & Outdoor",
              onTap: () {
                context.read<PageBloc>().add(
                    GoToSubcategoriesPage(GoToCategoriesPage(pageEvent), 7));
              },
            ),
            CustomListTile(
              "Buku & Alat Tulis",
              onTap: () {
                context.read<PageBloc>().add(
                    GoToSubcategoriesPage(GoToCategoriesPage(pageEvent), 8));
              },
            ),
            Container(
              height: 40,
              child: ListTile(
                title: Text("Lainnya",
                    style: blackTextFont.copyWith(fontSize: 14)),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
