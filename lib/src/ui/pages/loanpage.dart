part of 'pages.dart';

class LoanPage extends StatefulWidget {
  final PageEvent pageEvent;
  LoanPage(this.pageEvent, {Key key}) : super(key: key);

  @override
  _LoanPageState createState() => _LoanPageState();
}

class _LoanPageState extends State<LoanPage> {
  TextEditingController amountController = TextEditingController();
  int selectedAmount = 0;
  List<String> types;
  String selectedType;

  @override
  void initState() {
    super.initState();
    types = ['UMKM', 'Pendidikan'];
    // selectedType = types[0];
  }

  @override
  Widget build(BuildContext context) {
    BlocProvider.of<ThemeBloc>(context)
        .add(ChangeTheme(ThemeData().copyWith(primaryColor: mainColor)));

    return WillPopScope(
      onWillPop: () async {
        context.read<PageBloc>().add(widget.pageEvent);
        return;
      },
      child: GeneralPage(
        title: "Loan",
        onBackButtonPressed: () {
          context.read<PageBloc>().add(GoToMainPage());
        },
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              width: double.infinity,
              margin: EdgeInsets.all(defaultMargin),
              padding: EdgeInsets.symmetric(horizontal: 10),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(8),
                  border: Border.all(color: mainColor)),
              child: TextField(
                controller: amountController,
                keyboardType: TextInputType.number,
                onChanged: (text) {
                  String temp = '';
                  for (int i = 0; i <= text.length; i++) {
                    temp += text.isDigit(i) ? text[i] : '';
                  }
                  setState(() {
                    selectedAmount = int.tryParse(temp) ?? 0;
                  });
                  amountController.text = NumberFormat.currency(
                          locale: 'id_ID', symbol: 'IDR ', decimalDigits: 0)
                      .format(selectedAmount);
                  amountController.selection = TextSelection.fromPosition(
                      TextPosition(offset: amountController.text.length));
                },
                decoration: InputDecoration(
                    border: InputBorder.none,
                    hintStyle: greyTextFont,
                    hintText: 'Amount'),
              ),
            ),
            Container(
              width: double.infinity,
              margin: EdgeInsets.symmetric(horizontal: defaultMargin),
              padding: EdgeInsets.symmetric(horizontal: 10),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(8),
                  border: Border.all(color: mainColor)),
              child: DropdownButton(
                  value: selectedType,
                  isExpanded: true,
                  underline: SizedBox(),
                  hint: Text("Type Pinjaman", style: greyTextFont),
                  items: types
                      .map((e) => DropdownMenuItem(
                          value: e,
                          child: Text(
                            e,
                            style: blackTextFont,
                          )))
                      .toList(),
                  onChanged: (item) {
                    setState(() {
                      selectedType = item;
                    });
                  }),
            ),
            Container(
              width: MediaQuery.of(context).size.width / 1.4,
              margin: EdgeInsets.only(top: 24),
              height: 45,
              padding: EdgeInsets.symmetric(horizontal: defaultMargin),
              child: RaisedButton(
                onPressed: () {
                  context.read<PageBloc>().add(GoToLoanaPage(GoToMainPage()));
                },
                elevation: 0,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(8)),
                color: orangeColor,
                child: Text(
                  'Next',
                  style: blackTextFont.copyWith(fontSize: 16),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
