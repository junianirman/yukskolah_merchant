part of 'pages.dart';

class DeliveryServicePage extends StatefulWidget {
  final PageEvent pageEvent;

  DeliveryServicePage(this.pageEvent, {Key key}) : super(key: key);

  @override
  _DeliveryServicePageState createState() => _DeliveryServicePageState();
}

class _DeliveryServicePageState extends State<DeliveryServicePage> {
  bool isSelectedAnteraja = false;
  bool isSelectedNinjaExpress = false;
  bool isSelectedIdExpress = false;
  bool isSelectedSicepatReguler = false;
  bool isSelectedSicepatHalu = false;
  bool isSelectedJntExpress = false;
  bool isSelectedJneReguler = false;
  bool isSelectedJneYes = false;
  bool isSelectedJneJtr = false;
  bool isSelectedGrabExpressSameDay = false;
  bool isSelectedGosendSameDay = false;

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        context.read<PageBloc>().add(widget.pageEvent);
        return;
      },
      child: GeneralPage(
        title: "Jasa Kirim Saya",
        onBackButtonPressed: () {
          context.read<PageBloc>().add(widget.pageEvent);
        },
        child: ListView(
          children: [
            Container(
              margin: EdgeInsets.all(defaultMargin),
              child: Column(
                children: [
                  Container(
                    height: 32,
                    child: ListTile(
                      leading: Text("Anter Aja", style: blackTextFont),
                      trailing: Switch(
                        value: isSelectedAnteraja,
                        onChanged: (value) {
                          setState(() {
                            isSelectedAnteraja = value;
                          });
                        },
                        activeColor: greenColor,
                      ),
                    ),
                  ),
                  Container(
                    height: 32,
                    child: ListTile(
                      leading: Text("Ninja Express", style: blackTextFont),
                      trailing: Switch(
                        value: isSelectedNinjaExpress,
                        onChanged: (value) {
                          setState(() {
                            isSelectedNinjaExpress = value;
                          });
                        },
                        activeColor: greenColor,
                      ),
                    ),
                  ),
                  Container(
                    height: 32,
                    child: ListTile(
                      leading: Text("ID Express", style: blackTextFont),
                      trailing: Switch(
                        value: isSelectedIdExpress,
                        onChanged: (value) {
                          setState(() {
                            isSelectedIdExpress = value;
                          });
                        },
                        activeColor: greenColor,
                      ),
                    ),
                  ),
                  Container(
                    height: 32,
                    child: ListTile(
                      leading: Text("Sicepat Reguler", style: blackTextFont),
                      trailing: Switch(
                        value: isSelectedSicepatReguler,
                        onChanged: (value) {
                          setState(() {
                            isSelectedSicepatReguler = value;
                          });
                        },
                        activeColor: greenColor,
                      ),
                    ),
                  ),
                  Container(
                    height: 32,
                    child: ListTile(
                      leading: Text("Sicepat Halu", style: blackTextFont),
                      trailing: Switch(
                        value: isSelectedSicepatHalu,
                        onChanged: (value) {
                          setState(() {
                            isSelectedSicepatHalu = value;
                          });
                        },
                        activeColor: greenColor,
                      ),
                    ),
                  ),
                  Container(
                    height: 32,
                    child: ListTile(
                      leading: Text("J&T Express", style: blackTextFont),
                      trailing: Switch(
                        value: isSelectedJntExpress,
                        onChanged: (value) {
                          setState(() {
                            isSelectedJntExpress = value;
                          });
                        },
                        activeColor: greenColor,
                      ),
                    ),
                  ),
                  Container(
                    height: 32,
                    child: ListTile(
                      leading: Text("JNE Reguler", style: blackTextFont),
                      trailing: Switch(
                        value: isSelectedJneReguler,
                        onChanged: (value) {
                          setState(() {
                            isSelectedJneReguler = value;
                          });
                        },
                        activeColor: greenColor,
                      ),
                    ),
                  ),
                  Container(
                    height: 32,
                    child: ListTile(
                      leading: Text("JNE YES", style: blackTextFont),
                      trailing: Switch(
                        value: isSelectedJneYes,
                        onChanged: (value) {
                          setState(() {
                            isSelectedJneYes = value;
                          });
                        },
                        activeColor: greenColor,
                      ),
                    ),
                  ),
                  Container(
                    height: 32,
                    child: ListTile(
                      leading: Text("JNE JTR", style: blackTextFont),
                      trailing: Switch(
                        value: isSelectedJneJtr,
                        onChanged: (value) {
                          setState(() {
                            isSelectedJneJtr = value;
                          });
                        },
                        activeColor: greenColor,
                      ),
                    ),
                  ),
                  Container(
                    height: 32,
                    child: ListTile(
                      leading:
                          Text("GRAB Express SameDay", style: blackTextFont),
                      trailing: Switch(
                        value: isSelectedGrabExpressSameDay,
                        onChanged: (value) {
                          setState(() {
                            isSelectedGrabExpressSameDay = value;
                          });
                        },
                        activeColor: greenColor,
                      ),
                    ),
                  ),
                  Container(
                    height: 32,
                    child: ListTile(
                      leading: Text("GoSend SameDay", style: blackTextFont),
                      trailing: Switch(
                        value: isSelectedGosendSameDay,
                        onChanged: (value) {
                          setState(() {
                            isSelectedGosendSameDay = value;
                          });
                        },
                        activeColor: greenColor,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
