part of 'pages.dart';

class OrderPage extends StatefulWidget {
  final PageEvent pageEvent;

  const OrderPage(this.pageEvent, {Key key}) : super(key: key);

  @override
  _OrderPageState createState() => _OrderPageState();
}

class _OrderPageState extends State<OrderPage> {
  int selectedIndex = 0;

  @override
  Widget build(BuildContext context) {
    BlocProvider.of<ThemeBloc>(context)
        .add(ChangeTheme(ThemeData().copyWith(primaryColor: mainColor)));

    return WillPopScope(
      onWillPop: () {
        context.read<PageBloc>().add(widget.pageEvent);
        return;
      },
      child: GeneralPage(
        title: "Order",
        onBackButtonPressed: () {
          context.read<PageBloc>().add(widget.pageEvent);
        },
        child: Column(
          children: [
            CustomTabbar(
              titles: [
                "Dikemas",
                "Dikirim",
                "Selesai",
                "Pembatalan",
                "Pengembalian"
              ],
              selectedIndex: selectedIndex,
              onTap: (index) {
                setState(() {
                  selectedIndex = index;
                });
              },
            ),
            Expanded(
              child: ListView(
                  children: dummyTransaction
                      .map(
                        (e) => selectedIndex == 0 &&
                                e.orderTab == OrderTab.dikemas
                            ? Container(
                                margin: EdgeInsets.fromLTRB(
                                  defaultMargin,
                                  defaultMargin,
                                  defaultMargin,
                                  (dummyTransaction.indexOf(e) ==
                                          dummyTransaction.length - 1
                                      ? defaultMargin
                                      : 0),
                                ),
                                height:
                                    MediaQuery.of(context).size.height / 3.2 +
                                        8,
                                decoration: BoxDecoration(
                                  color: greyColor,
                                  borderRadius: BorderRadius.circular(8),
                                ),
                                child: DikemasCard(e),
                              )
                            : selectedIndex == 1 &&
                                    e.orderTab == OrderTab.dikirim
                                ? Container(
                                    margin: EdgeInsets.fromLTRB(
                                      defaultMargin,
                                      defaultMargin,
                                      defaultMargin,
                                      (dummyTransaction.indexOf(e) ==
                                              dummyTransaction.length - 1
                                          ? defaultMargin
                                          : 0),
                                    ),
                                    height: MediaQuery.of(context).size.height /
                                            1.6 +
                                        24,
                                    decoration: BoxDecoration(
                                      color: greyColor,
                                      borderRadius: BorderRadius.circular(8),
                                    ),
                                    child: DikirimCard(e),
                                  )
                                : selectedIndex == 2 &&
                                        e.orderTab == OrderTab.selesai
                                    ? Container(
                                        margin: EdgeInsets.fromLTRB(
                                          defaultMargin,
                                          defaultMargin,
                                          defaultMargin,
                                          (dummyTransaction.indexOf(e) ==
                                                  dummyTransaction.length - 1
                                              ? defaultMargin
                                              : 0),
                                        ),
                                        height:
                                            MediaQuery.of(context).size.height /
                                                    1.6 -
                                                28,
                                        decoration: BoxDecoration(
                                          color: greyColor,
                                          borderRadius:
                                              BorderRadius.circular(8),
                                        ),
                                        child: SelesaiCard(e),
                                      )
                                    : selectedIndex == 3 &&
                                            e.orderTab == OrderTab.pembatalan
                                        ? Container(
                                            margin: EdgeInsets.fromLTRB(
                                              defaultMargin,
                                              defaultMargin,
                                              defaultMargin,
                                              (dummyTransaction.indexOf(e) ==
                                                      dummyTransaction.length -
                                                          1
                                                  ? defaultMargin
                                                  : 0),
                                            ),
                                            height: MediaQuery.of(context)
                                                    .size
                                                    .height /
                                                3.2,
                                            decoration: BoxDecoration(
                                              color: greyColor,
                                              borderRadius:
                                                  BorderRadius.circular(8),
                                            ),
                                            child: CancelCard(e),
                                          )
                                        : selectedIndex == 4 &&
                                                e.orderTab ==
                                                    OrderTab.pengembalian
                                            ? Container(
                                                margin: EdgeInsets.fromLTRB(
                                                  defaultMargin,
                                                  defaultMargin,
                                                  defaultMargin,
                                                  (dummyTransaction
                                                              .indexOf(e) ==
                                                          dummyTransaction
                                                                  .length -
                                                              1
                                                      ? defaultMargin
                                                      : 0),
                                                ),
                                                height: MediaQuery.of(context)
                                                            .size
                                                            .height /
                                                        1.24 +
                                                    12,
                                                decoration: BoxDecoration(
                                                  color: greyColor,
                                                  borderRadius:
                                                      BorderRadius.circular(8),
                                                ),
                                                child: ReturnCard(e),
                                              )
                                            : Container(),
                      )
                      .toList()),
            ),
          ],
        ),
      ),
    );
  }
}
