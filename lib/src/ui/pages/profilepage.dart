part of 'pages.dart';

class ProfilePage extends StatelessWidget {
  const ProfilePage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    BlocProvider.of<ThemeBloc>(context)
        .add(ChangeTheme(ThemeData().copyWith(primaryColor: mainColor)));

    return SafeArea(
      child: Container(
        width: double.infinity,
        color: Colors.white,
        child: Column(
          children: [
            Container(
              margin: EdgeInsets.all(defaultMargin),
              child: Row(
                children: [
                  GestureDetector(
                    onTap: () {
                      context.read<PageBloc>().add(GoToEditProfilePage(
                          GoToMainPage(bottomNavBarIndex: 3)));
                    },
                    child: Container(
                      width: 90,
                      height: 90,
                      margin: EdgeInsets.symmetric(horizontal: 12),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(8),
                        image: DecorationImage(
                            image: AssetImage(dummyUser.picturePath),
                            fit: BoxFit.cover),
                      ),
                    ),
                  ),
                  Container(
                    height: 90,
                    width: MediaQuery.of(context).size.width / 2,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Text(
                          dummyUser.name,
                          style: blackTextFont.copyWith(
                              fontWeight: FontWeight.bold),
                          maxLines: 1,
                          overflow: TextOverflow.clip,
                        ),
                        SizedBox(height: 8),
                        Row(
                          children: [
                            Text(
                              "Pengikut ${dummyUser.follow.follower}",
                              style: blackTextFont,
                            ),
                            SizedBox(width: 8),
                            Text(
                              "|",
                              style: blackTextFont,
                            ),
                            SizedBox(width: 8),
                            Text(
                              "Mengikuti ${dummyUser.follow.following}",
                              style: blackTextFont,
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(height: 50),
            Container(
              height: 132,
              margin: EdgeInsets.all(defaultMargin),
              decoration: BoxDecoration(
                color: darkGreyColor,
                borderRadius: BorderRadius.circular(8),
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  ListTile(
                    leading: Text("Produk Saya", style: blackTextFont),
                    trailing: Icon(Icons.arrow_forward_ios),
                    onTap: () {
                      context.read<PageBloc>().add(
                          GoToProductPage(GoToMainPage(bottomNavBarIndex: 3)));
                    },
                  ),
                  Container(
                    height: 20,
                    padding: EdgeInsets.symmetric(horizontal: defaultMargin),
                    child:
                        Divider(thickness: 1, color: Colors.black, indent: 0),
                  ),
                  ListTile(
                    leading: Text("Tambah Produk", style: blackTextFont),
                    trailing: Icon(Icons.add),
                    onTap: () {
                      context.read<PageBloc>().add(GoToAddProductPage(
                          GoToMainPage(bottomNavBarIndex: 3)));
                    },
                  ),
                ],
              ),
            ),
            CustomListTile("Saldo Penjual", onTap: () {
              context
                  .read<PageBloc>()
                  .add(GoToSaldoPage(GoToMainPage(bottomNavBarIndex: 3)));
            }),
            CustomListTile("Jasa Kirim Saya", onTap: () {
              context.read<PageBloc>().add(
                  GoToDeliveryServicePage(GoToMainPage(bottomNavBarIndex: 3)));
            }),
            CustomListTile("Penilaian Toko", onTap: () {
              context.read<PageBloc>().add(
                  GoToStoreAppraisalPage(GoToMainPage(bottomNavBarIndex: 3)));
            }),
            CustomListTile("Bisnis Saya", onTap: () {
              context
                  .read<PageBloc>()
                  .add(GoToBusinessPage(GoToMainPage(bottomNavBarIndex: 3)));
            }),
          ],
        ),
      ),
    );
  }
}
