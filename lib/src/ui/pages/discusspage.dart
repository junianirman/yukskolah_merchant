part of 'pages.dart';

class DiscussPage extends StatelessWidget {
  final PageEvent pageEvent;

  const DiscussPage(this.pageEvent, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        context.read<PageBloc>().add(pageEvent);
        return;
      },
      child: GeneralPage(
        title: "Discuss",
        onBackButtonPressed: () {
          context.read<PageBloc>().add(pageEvent);
        },
        child: ListView(
          children: dummyDiscuss
              .map(
                (e) => Container(
                  margin: EdgeInsets.fromLTRB(
                    defaultMargin,
                    defaultMargin,
                    defaultMargin,
                    (dummyDiscuss.indexOf(e) == dummyDiscuss.length - 1
                        ? defaultMargin
                        : 0),
                  ),
                  height: MediaQuery.of(context).size.height / 2.4 + 24,
                  decoration: BoxDecoration(
                    color: greyColor,
                    borderRadius: BorderRadius.circular(8),
                  ),
                  child: DiscussCard(e),
                ),
              )
              .toList(),
        ),
      ),
    );
  }
}
