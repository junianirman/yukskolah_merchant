part of 'pages.dart';

class PromoPage extends StatelessWidget {
  final PageEvent pageEvent;

  const PromoPage(this.pageEvent, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        context.read<PageBloc>().add(pageEvent);
        return;
      },
      child: GeneralPage(
        title: "Promo",
        onBackButtonPressed: () {
          context.read<PageBloc>().add(pageEvent);
        },
        child: Stack(
          children: [
            ListView(
              children: [
                Container(
                  margin: EdgeInsets.all(defaultMargin),
                  child: Column(
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                "List Promo",
                                style: blackTextFont,
                              ),
                              Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: dummyPromo
                                      .map(
                                        (e) => Text(
                                          e.promoCode,
                                          style: blackTextFont,
                                        ),
                                      )
                                      .toList())
                            ],
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                "Potongan",
                                style: blackTextFont,
                              ),
                              Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: dummyPromo
                                      .map(
                                        (e) => Text(
                                          e.promoType == PromoType.presentase
                                              ? "${e.discount}%"
                                              : NumberFormat.currency(
                                                      locale: "id_ID",
                                                      decimalDigits: 0,
                                                      symbol: "Rp ")
                                                  .format(e.discount),
                                          style: blackTextFont,
                                        ),
                                      )
                                      .toList())
                            ],
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                "Quota",
                                style: blackTextFont,
                              ),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: dummyPromo
                                    .map(
                                      (e) => Text(
                                        "${e.quota}",
                                        style: blackTextFont,
                                      ),
                                    )
                                    .toList(),
                              )
                            ],
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                "Berlaku Sampai",
                                style: blackTextFont,
                              ),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: dummyPromo
                                    .map(
                                      (e) => Text(
                                        DateFormat('dd-MM-yyyy')
                                            .format(e.expiredDate),
                                        style: blackTextFont,
                                      ),
                                    )
                                    .toList(),
                              )
                            ],
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ],
            ),
            Align(
              alignment: Alignment.bottomRight,
              child: Container(
                margin: EdgeInsets.all(defaultMargin),
                child: FloatingActionButton(
                  onPressed: () {
                    context
                        .read<PageBloc>()
                        .add(GoToAddPromoPage(GoToPromoPage(pageEvent)));
                  },
                  child: Container(
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      image: DecorationImage(
                        image: AssetImage('assets/ic_plus.png'),
                      ),
                    ),
                  ),
                  elevation: 0,
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
