part of 'pages.dart';

class AddPromoPage extends StatefulWidget {
  final PageEvent pageEvent;

  AddPromoPage(this.pageEvent, {Key key}) : super(key: key);

  @override
  _AddPromoPageState createState() => _AddPromoPageState();
}

class _AddPromoPageState extends State<AddPromoPage> {
  TextEditingController expiredDateController = TextEditingController();
  TextEditingController promoController = TextEditingController();
  TextEditingController discountController = TextEditingController();
  TextEditingController quotaController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        context.read<PageBloc>().add(widget.pageEvent);
        return;
      },
      child: GeneralPage(
        title: "Promo",
        onBackButtonPressed: () {
          context.read<PageBloc>().add(widget.pageEvent);
        },
        child: ListView(
          children: [
            Container(
              margin: EdgeInsets.all(defaultMargin),
              child: Column(
                children: [
                  Row(
                    children: [
                      Container(
                        width: MediaQuery.of(context).size.width / 1.6,
                        padding: EdgeInsets.symmetric(horizontal: 10),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(8),
                            border: Border.all(color: mainColor)),
                        child: TextField(
                          controller: expiredDateController,
                          decoration: InputDecoration(
                            border: InputBorder.none,
                            hintStyle: greyTextFont,
                            hintText: "Berlaku Sampai Date",
                            suffixIcon: Icon(Icons.date_range),
                          ),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: 8),
                  Container(
                    width: double.infinity,
                    padding: EdgeInsets.symmetric(horizontal: 10),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(8),
                        border: Border.all(color: mainColor)),
                    child: TextField(
                      controller: promoController,
                      decoration: InputDecoration(
                        border: InputBorder.none,
                        hintStyle: greyTextFont,
                        hintText: "Promo",
                      ),
                    ),
                  ),
                  SizedBox(height: 8),
                  Container(
                    width: double.infinity,
                    padding: EdgeInsets.symmetric(horizontal: 10),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(8),
                        border: Border.all(color: mainColor)),
                    child: TextField(
                      controller: discountController,
                      decoration: InputDecoration(
                        border: InputBorder.none,
                        hintStyle: greyTextFont,
                        hintText: "Potongan",
                      ),
                    ),
                  ),
                  SizedBox(height: 8),
                  Container(
                    width: double.infinity,
                    padding: EdgeInsets.symmetric(horizontal: 10),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(8),
                        border: Border.all(color: mainColor)),
                    child: TextField(
                      controller: quotaController,
                      decoration: InputDecoration(
                        border: InputBorder.none,
                        hintStyle: greyTextFont,
                        hintText: "Quota",
                      ),
                    ),
                  ),
                  SizedBox(height: 8),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        Container(
                          width: MediaQuery.of(context).size.width / 2.4,
                          height: 45,
                          child: RaisedButton(
                            onPressed: () {},
                            elevation: 0,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(8)),
                            color: darkGreyColor,
                            child: Text(
                              'Generate Code',
                              style: blackTextFont,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
