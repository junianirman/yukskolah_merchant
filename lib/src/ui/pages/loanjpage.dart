part of 'pages.dart';

class LoanjPage extends StatefulWidget {
  final PageEvent pageEvent;

  LoanjPage(this.pageEvent, {Key key}) : super(key: key);

  @override
  _LoanjPageState createState() => _LoanjPageState();
}

class _LoanjPageState extends State<LoanjPage> {
  TextEditingController pinjamanController = TextEditingController();
  int selectedAmount = 0;

  @override
  Widget build(BuildContext context) {
    BlocProvider.of<ThemeBloc>(context)
        .add(ChangeTheme(ThemeData().copyWith(primaryColor: mainColor)));

    return WillPopScope(
      onWillPop: () {
        context.read<PageBloc>().add(widget.pageEvent);
        return;
      },
      child: GeneralPage(
        title: "Loan",
        onBackButtonPressed: () {
          context.read<PageBloc>().add(GoToMainPage());
        },
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              margin: EdgeInsets.only(bottom: 8),
              child: Text("Jumlah pinjaman ?",
                  style: blackTextFont.copyWith(fontSize: 16)),
            ),
            Container(
              width: double.infinity,
              margin: EdgeInsets.all(defaultMargin),
              padding: EdgeInsets.symmetric(horizontal: 10),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(8),
                  border: Border.all(color: mainColor)),
              child: TextField(
                controller: pinjamanController,
                keyboardType: TextInputType.number,
                onChanged: (text) {
                  String temp = '';
                  for (int i = 0; i <= text.length; i++) {
                    temp += text.isDigit(i) ? text[i] : '';
                  }
                  setState(() {
                    selectedAmount = int.tryParse(temp) ?? 0;
                  });
                  pinjamanController.text = NumberFormat.currency(
                          locale: 'id_ID', symbol: 'IDR ', decimalDigits: 0)
                      .format(selectedAmount);
                  pinjamanController.selection = TextSelection.fromPosition(
                      TextPosition(offset: pinjamanController.text.length));
                },
                decoration: InputDecoration(
                  border: InputBorder.none,
                ),
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width / 1.4,
              margin: EdgeInsets.only(top: 8),
              height: 45,
              padding: EdgeInsets.symmetric(horizontal: defaultMargin),
              child: RaisedButton(
                onPressed: () {
                  context.read<PageBloc>().add(GoToLoankPage(widget.pageEvent));
                },
                elevation: 0,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(8)),
                color: orangeColor,
                child: Text(
                  'Next',
                  style: blackTextFont.copyWith(fontSize: 16),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
