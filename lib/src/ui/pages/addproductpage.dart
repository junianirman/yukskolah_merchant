part of 'pages.dart';

class AddProductPage extends StatefulWidget {
  final PageEvent pageEvent;

  AddProductPage(this.pageEvent, {Key key}) : super(key: key);

  @override
  _AddProductPageState createState() => _AddProductPageState();
}

class _AddProductPageState extends State<AddProductPage> {
  TextEditingController productNameController = TextEditingController();
  TextEditingController productDescController = TextEditingController();
  bool isSelected = false;
  int harga = 0;
  int stock = 0;

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        context.read<PageBloc>().add(widget.pageEvent);
        return;
      },
      child: GeneralPage(
        title: "Tambah Produk",
        onBackButtonPressed: () {
          context.read<PageBloc>().add(widget.pageEvent);
        },
        child: ListView(
          children: [
            Container(
              margin: EdgeInsets.all(defaultMargin),
              child: Column(
                children: [
                  Row(
                    children: [
                      Container(
                        width: MediaQuery.of(context).size.width / 3.2 - 24,
                        height: 100,
                        decoration: BoxDecoration(
                          color: greyColor,
                          borderRadius: BorderRadius.circular(8),
                        ),
                        child: Icon(Icons.photo_camera_rounded, size: 48),
                      ),
                    ],
                  ),
                  SizedBox(height: 16),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    child: Text(
                      "Nama Produk",
                      style: blackTextFont,
                    ),
                  ),
                  SizedBox(height: 4),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    padding: EdgeInsets.symmetric(horizontal: 10),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8),
                      border: Border.all(color: mainColor),
                    ),
                    child: TextField(
                      controller: productNameController,
                      decoration: InputDecoration(border: InputBorder.none),
                    ),
                  ),
                  SizedBox(height: 8),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    child: Text(
                      "Deskripsi Produk",
                      style: blackTextFont,
                    ),
                  ),
                  SizedBox(height: 4),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    padding: EdgeInsets.symmetric(horizontal: 10),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8),
                      border: Border.all(color: mainColor),
                    ),
                    child: TextField(
                      controller: productDescController,
                      decoration: InputDecoration(border: InputBorder.none),
                    ),
                  ),
                  SizedBox(height: 8),
                  CustomListTileIcon(
                    "Kategori",
                    onTap: () {
                      context
                          .read<PageBloc>()
                          .add(GoToCategoriesPage(widget.pageEvent));
                    },
                  ),
                  Container(
                    height: 36,
                    child: ListTile(
                      leading: Icon(Icons.list_alt),
                      title: Transform.translate(
                        offset: Offset(-16, 0),
                        child: Text("Harga", style: blackTextFont),
                      ),
                      trailing: Text(
                        NumberFormat.currency(
                                locale: "id_ID",
                                decimalDigits: 0,
                                symbol: "Rp ")
                            .format(harga),
                        style:
                            blackTextFont.copyWith(fontWeight: FontWeight.w400),
                      ),
                      contentPadding: EdgeInsets.symmetric(horizontal: 0),
                    ),
                  ),
                  Container(
                    height: 36,
                    child: ListTile(
                      leading: Icon(Icons.list_alt),
                      title: Transform.translate(
                        offset: Offset(-16, 0),
                        child: Text("Stock", style: blackTextFont),
                      ),
                      trailing: Text(
                        "$stock",
                        style: blackTextFont,
                      ),
                      contentPadding: EdgeInsets.symmetric(horizontal: 0),
                    ),
                  ),
                  CustomListTileIcon(
                    "Variasi",
                    onTap: () {
                      context.read<PageBloc>().add(GoToVariasiPage(
                          GoToAddProductPage(widget.pageEvent)));
                    },
                  ),
                  CustomListTileIcon(
                    "Grosir",
                    onTap: () {
                      context.read<PageBloc>().add(
                          GoToGrosirPage(GoToAddProductPage(widget.pageEvent)));
                    },
                  ),
                  CustomListTileIcon(
                    "Ongkos Kirim",
                    onTap: () {
                      context.read<PageBloc>().add(GoToOngkisKirimPage(
                          GoToAddProductPage(widget.pageEvent)));
                    },
                  ),
                  CustomListTileIcon(
                    "Kondisi",
                    onTap: () {
                      context
                          .read<PageBloc>()
                          .add(GoToCategoriesPage(widget.pageEvent));
                    },
                  ),
                  Container(
                    height: 36,
                    child: ListTile(
                      leading: Icon(Icons.list_alt),
                      title: Transform.translate(
                        offset: Offset(-16, 0),
                        child: Text("Pre-Order", style: blackTextFont),
                      ),
                      trailing: Switch(
                        value: isSelected,
                        onChanged: (value) {
                          setState(() {
                            isSelected = value;
                          });
                        },
                        activeColor: greenColor,
                      ),
                      contentPadding: EdgeInsets.symmetric(horizontal: 0),
                    ),
                  ),
                  CustomListTileIcon(
                    "Etalase",
                    onTap: () {
                      context.read<PageBloc>().add(GoToEtalasePage(
                          GoToAddProductPage(widget.pageEvent)));
                    },
                  ),
                  SizedBox(height: 72),
                  Container(
                    width: MediaQuery.of(context).size.width / 3.2,
                    height: 45,
                    child: RaisedButton(
                      onPressed: () {},
                      elevation: 0,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(8)),
                      color: orangeColor,
                      child: Text(
                        'Simpan',
                        style: blackTextFont,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
