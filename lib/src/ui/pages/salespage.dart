part of 'pages.dart';

class SalesPage extends StatelessWidget {
  const SalesPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    BlocProvider.of<ThemeBloc>(context)
        .add(ChangeTheme(ThemeData().copyWith(primaryColor: mainColor)));

    return SafeArea(
      child: Container(
        width: double.infinity,
        color: Colors.white,
        child: Container(
          margin: EdgeInsets.all(defaultMargin),
          child: Column(
            children: [
              SalesCard("Confirm", dummySales.confirm),
              SalesCard("Process", dummySales.process),
              SalesCard("Send", dummySales.send),
              SalesCard("Complain", dummySales.complain),
              SalesCard("Delivered", dummySales.delivered),
              SalesCard("Cancel", dummySales.cancel),
            ],
          ),
        ),
      ),
    );
  }
}
