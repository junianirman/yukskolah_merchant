part of 'pages.dart';

class LoandPage extends StatefulWidget {
  final PageEvent pageEvent;

  LoandPage(this.pageEvent, {Key key}) : super(key: key);

  @override
  _LoandPageState createState() => _LoandPageState();
}

class _LoandPageState extends State<LoandPage> {
  List<String> pekerjaan;
  String selectedPekerjaan;

  @override
  void initState() {
    super.initState();
    pekerjaan = [
      'Pegawai Swasta',
      'Wiraswasta',
      'Profesional',
      'Ibu Rumah Tangga',
      'Pegawai Negeri',
      'TNI atau Polri'
    ];
    // selectedPekerjaan = pekerjaan[0];
  }

  @override
  Widget build(BuildContext context) {
    BlocProvider.of<ThemeBloc>(context)
        .add(ChangeTheme(ThemeData().copyWith(primaryColor: mainColor)));

    return WillPopScope(
      onWillPop: () {
        context.read<PageBloc>().add(widget.pageEvent);
        return;
      },
      child: GeneralPage(
        title: "Loan",
        onBackButtonPressed: () {
          context.read<PageBloc>().add(widget.pageEvent);
        },
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              margin: EdgeInsets.only(bottom: 8),
              child: Text("Apa pekerjaan anda ?",
                  style: blackTextFont.copyWith(fontSize: 16)),
            ),
            Container(
              width: double.infinity,
              margin: EdgeInsets.all(defaultMargin),
              padding: EdgeInsets.symmetric(horizontal: 10),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(8),
                  border: Border.all(color: mainColor)),
              child: DropdownButton(
                  value: selectedPekerjaan,
                  hint: Text("Pekerjaan", style: greyTextFont),
                  isExpanded: true,
                  underline: SizedBox(),
                  items: pekerjaan
                      .map((e) => DropdownMenuItem(
                          value: e,
                          child: Text(
                            e,
                            style: blackTextFont,
                          )))
                      .toList(),
                  onChanged: (item) {
                    setState(() {
                      selectedPekerjaan = item;
                    });
                  }),
            ),
            Container(
              width: MediaQuery.of(context).size.width / 1.4,
              margin: EdgeInsets.only(top: 8),
              height: 45,
              padding: EdgeInsets.symmetric(horizontal: defaultMargin),
              child: RaisedButton(
                onPressed: () {
                  context.read<PageBloc>().add(GoToLoanePage(widget.pageEvent));
                },
                elevation: 0,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(8)),
                color: orangeColor,
                child: Text(
                  'Next',
                  style: blackTextFont.copyWith(fontSize: 16),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
