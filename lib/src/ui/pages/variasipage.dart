part of 'pages.dart';

class VariasiPage extends StatefulWidget {
  final PageEvent pageEvent;

  const VariasiPage(this.pageEvent, {Key key}) : super(key: key);

  @override
  _VariasiPageState createState() => _VariasiPageState();
}

class _VariasiPageState extends State<VariasiPage> {
  bool isSelected = false;

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        context.read<PageBloc>().add(widget.pageEvent);
        return;
      },
      child: GeneralPage(
        title: "Variasi Produk",
        onBackButtonPressed: () {
          context.read<PageBloc>().add(widget.pageEvent);
        },
        child: ListView(
          children: [
            Container(
              margin: EdgeInsets.all(defaultMargin),
              padding: EdgeInsets.all(8),
              decoration: BoxDecoration(
                  color: greyColor, borderRadius: BorderRadius.circular(8)),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text("Warna", style: blackTextFont),
                  Divider(thickness: 1, color: Colors.black),
                  Container(
                    width: MediaQuery.of(context).size.width / 3.2,
                    height: 40,
                    child: RaisedButton(
                      onPressed: () {},
                      elevation: 0,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(8)),
                      color: darkGreyColor,
                      child: Text(
                        'Tambah Warna',
                        style: blackTextFont,
                      ),
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text("Tambah Foto Ke Variasi Warna",
                          style: blackTextFont),
                      Switch(
                        value: isSelected,
                        onChanged: (value) {
                          setState(() {
                            isSelected = value;
                          });
                        },
                        activeColor: greenColor,
                      ),
                    ],
                  ),
                  Text("Semua foto harus di upload apabila di aktifkan",
                      style: blackTextFont)
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.symmetric(horizontal: defaultMargin),
              padding: EdgeInsets.all(8),
              decoration: BoxDecoration(
                  color: greyColor, borderRadius: BorderRadius.circular(8)),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text("Ukuran", style: blackTextFont),
                  Divider(thickness: 1, color: Colors.black),
                  Container(
                    width: MediaQuery.of(context).size.width / 3.2,
                    height: 40,
                    child: RaisedButton(
                      onPressed: () {},
                      elevation: 0,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(8)),
                      color: darkGreyColor,
                      child: Text(
                        'Tambah Ukuran',
                        style: blackTextFont,
                      ),
                    ),
                  ),
                  SizedBox(height: defaultMargin),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                        width: MediaQuery.of(context).size.width / 3.2,
                        height: 100,
                        decoration: BoxDecoration(
                          color: darkGreyColor,
                          borderRadius: BorderRadius.circular(8),
                        ),
                        child: Icon(Icons.photo_camera_rounded, size: 48),
                      ),
                    ],
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
