part of 'pages.dart';

class ComplainPage extends StatefulWidget {
  final PageEvent pageEvent;

  ComplainPage(this.pageEvent, {Key key}) : super(key: key);

  @override
  _ComplainPageState createState() => _ComplainPageState();
}

class _ComplainPageState extends State<ComplainPage> {
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        context.read<PageBloc>().add(widget.pageEvent);
        return;
      },
      child: GeneralPage(
        title: "Complain",
        onBackButtonPressed: () {
          context.read<PageBloc>().add(widget.pageEvent);
        },
        child: ListView(
          children: dummyTransaction
              .where((element) => element.status == "Terkirim")
              .toList()
              .map(
                (e) => Container(
                  margin: EdgeInsets.fromLTRB(
                    defaultMargin,
                    defaultMargin,
                    defaultMargin,
                    (dummyTransaction.indexOf(e) == dummyTransaction.length - 1
                        ? defaultMargin
                        : 0),
                  ),
                  height: MediaQuery.of(context).size.height / 1.6 - 22,
                  decoration: BoxDecoration(
                    color: greyColor,
                    borderRadius: BorderRadius.circular(8),
                  ),
                  child: ComplainCard(e),
                ),
              )
              .toList(),
        ),
      ),
    );
  }
}
