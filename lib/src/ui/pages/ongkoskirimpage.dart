part of 'pages.dart';

class OngkosKirimPage extends StatefulWidget {
  final PageEvent pageEvent;

  OngkosKirimPage(this.pageEvent, {Key key}) : super(key: key);

  @override
  _OngkosKirimPageState createState() => _OngkosKirimPageState();
}

class _OngkosKirimPageState extends State<OngkosKirimPage> {
  bool isSelectedJne = false;
  bool isSelectedJnt = false;
  bool isSelectedSicepat = false;
  bool isSelectedJneOngkosKirim = false;
  bool isSelectedJntOngkosKirim = false;
  bool isSelectedSicepatOngkosKirim = false;

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        context.read<PageBloc>().add(widget.pageEvent);
        return;
      },
      child: GeneralPage(
        title: "Ongkos Kirim",
        onBackButtonPressed: () {
          context.read<PageBloc>().add(widget.pageEvent);
        },
        child: ListView(
          children: [
            Container(
              margin: EdgeInsets.all(defaultMargin),
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text("Berat Produk", style: blackTextFont),
                      Text("Atur Berat", style: greyTextFont)
                    ],
                  ),
                  Divider(thickness: 1, color: Colors.black),
                  SizedBox(height: 8),
                  Row(
                    children: [
                      Text("Ukuran Paket", style: blackTextFont),
                    ],
                  ),
                  SizedBox(height: 4),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text("Lebar (cm)", style: blackTextFont),
                      Text("Atur Lebar", style: greyTextFont)
                    ],
                  ),
                  SizedBox(height: 4),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text("Panjang (cm)", style: blackTextFont),
                      Text("Atur Panjang", style: greyTextFont)
                    ],
                  ),
                  Divider(thickness: 1, color: Colors.black),
                  SizedBox(height: 8),
                  Container(
                    padding: EdgeInsets.fromLTRB(8, 8, 0, 8),
                    decoration: BoxDecoration(
                      color: greyColor,
                      borderRadius: BorderRadius.circular(8),
                    ),
                    child: Column(
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text("JNE Reguler (Cashless)",
                                style: blackTextFont),
                            Container(
                              height: 32,
                              child: Switch(
                                value: isSelectedJne,
                                onChanged: (value) {
                                  setState(() {
                                    isSelectedJne = value;
                                  });
                                },
                                activeColor: greenColor,
                              ),
                            ),
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text("Saya akan menanggung ongkos kirim",
                                style: blackTextFont),
                            Container(
                              height: 32,
                              child: Switch(
                                value: isSelectedJneOngkosKirim,
                                onChanged: (value) {
                                  setState(() {
                                    isSelectedJneOngkosKirim = value;
                                  });
                                },
                                activeColor: greenColor,
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                  SizedBox(height: 8),
                  Container(
                    padding: EdgeInsets.fromLTRB(8, 8, 0, 8),
                    decoration: BoxDecoration(
                      color: greyColor,
                      borderRadius: BorderRadius.circular(8),
                    ),
                    child: Column(
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text("J&T Express", style: blackTextFont),
                            Container(
                              height: 32,
                              child: Switch(
                                value: isSelectedJnt,
                                onChanged: (value) {
                                  setState(() {
                                    isSelectedJnt = value;
                                  });
                                },
                                activeColor: greenColor,
                              ),
                            ),
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text("Saya akan menanggung ongkos kirim",
                                style: blackTextFont),
                            Container(
                              height: 32,
                              child: Switch(
                                value: isSelectedJntOngkosKirim,
                                onChanged: (value) {
                                  setState(() {
                                    isSelectedJntOngkosKirim = value;
                                  });
                                },
                                activeColor: greenColor,
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                  SizedBox(height: 8),
                  Container(
                    padding: EdgeInsets.fromLTRB(8, 8, 0, 8),
                    decoration: BoxDecoration(
                      color: greyColor,
                      borderRadius: BorderRadius.circular(8),
                    ),
                    child: Column(
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text("Sicepat Reguler", style: blackTextFont),
                            Container(
                              height: 32,
                              child: Switch(
                                value: isSelectedSicepat,
                                onChanged: (value) {
                                  setState(() {
                                    isSelectedSicepat = value;
                                  });
                                },
                                activeColor: greenColor,
                              ),
                            ),
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text("Saya akan menanggung ongkos kirim",
                                style: blackTextFont),
                            Container(
                              height: 32,
                              child: Switch(
                                value: isSelectedSicepatOngkosKirim,
                                onChanged: (value) {
                                  setState(() {
                                    isSelectedSicepatOngkosKirim = value;
                                  });
                                },
                                activeColor: greenColor,
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
