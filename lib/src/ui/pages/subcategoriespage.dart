part of 'pages.dart';

class SubcategoriesPage extends StatelessWidget {
  final PageEvent pageEvent;
  final int categoriesIndex;

  const SubcategoriesPage(this.pageEvent, this.categoriesIndex, {Key key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        context.read<PageBloc>().add(pageEvent);
        return;
      },
      child: GeneralPage(
        title: "Kategori",
        onBackButtonPressed: () {
          context.read<PageBloc>().add(pageEvent);
        },
        child: ListView(
          children: categoriesIndex == 1
              ? dummyCategories.foodBeverage
                  .map(
                    (e) => Container(
                      margin: EdgeInsets.fromLTRB(
                        defaultMargin,
                        defaultMargin,
                        defaultMargin,
                        (dummyCategories.foodBeverage.indexOf(e) ==
                                dummyCategories.foodBeverage.length - 1
                            ? defaultMargin
                            : 0),
                      ),
                      child: Text(
                        e.name,
                        style: blackTextFont,
                      ),
                    ),
                  )
                  .toList()
              : categoriesIndex == 2
                  ? dummyCategories.fashionMuslim
                      .map(
                        (e) => Container(
                          margin: EdgeInsets.fromLTRB(
                            defaultMargin,
                            defaultMargin,
                            defaultMargin,
                            (dummyCategories.fashionMuslim.indexOf(e) ==
                                    dummyCategories.fashionMuslim.length - 1
                                ? defaultMargin
                                : 0),
                          ),
                          child: Text(
                            e.name,
                            style: blackTextFont,
                          ),
                        ),
                      )
                      .toList()
                  : categoriesIndex == 3
                      ? dummyCategories.pakaianPria
                          .map(
                            (e) => Container(
                              margin: EdgeInsets.fromLTRB(
                                defaultMargin,
                                defaultMargin,
                                defaultMargin,
                                (dummyCategories.pakaianPria.indexOf(e) ==
                                        dummyCategories.pakaianPria.length - 1
                                    ? defaultMargin
                                    : 0),
                              ),
                              child: Text(
                                e.name,
                                style: blackTextFont,
                              ),
                            ),
                          )
                          .toList()
                      : categoriesIndex == 4
                          ? dummyCategories.pakaianWanita
                              .map(
                                (e) => Container(
                                  margin: EdgeInsets.fromLTRB(
                                    defaultMargin,
                                    defaultMargin,
                                    defaultMargin,
                                    (dummyCategories.pakaianWanita.indexOf(e) ==
                                            dummyCategories
                                                    .pakaianWanita.length -
                                                1
                                        ? defaultMargin
                                        : 0),
                                  ),
                                  child: Text(
                                    e.name,
                                    style: blackTextFont,
                                  ),
                                ),
                              )
                              .toList()
                          : categoriesIndex == 5
                              ? dummyCategories.fashionAnak
                                  .map(
                                    (e) => Container(
                                      margin: EdgeInsets.fromLTRB(
                                        defaultMargin,
                                        defaultMargin,
                                        defaultMargin,
                                        (dummyCategories.fashionAnak
                                                    .indexOf(e) ==
                                                dummyCategories
                                                        .fashionAnak.length -
                                                    1
                                            ? defaultMargin
                                            : 0),
                                      ),
                                      child: Text(
                                        e.name,
                                        style: blackTextFont,
                                      ),
                                    ),
                                  )
                                  .toList()
                              : categoriesIndex == 6
                                  ? dummyCategories.komputerAksesoris
                                      .map(
                                        (e) => Container(
                                          margin: EdgeInsets.fromLTRB(
                                            defaultMargin,
                                            defaultMargin,
                                            defaultMargin,
                                            (dummyCategories.komputerAksesoris
                                                        .indexOf(e) ==
                                                    dummyCategories
                                                            .komputerAksesoris
                                                            .length -
                                                        1
                                                ? defaultMargin
                                                : 0),
                                          ),
                                          child: Text(
                                            e.name,
                                            style: blackTextFont,
                                          ),
                                        ),
                                      )
                                      .toList()
                                  : categoriesIndex == 7
                                      ? dummyCategories.olahragaOutdoor
                                          .map(
                                            (e) => Container(
                                              margin: EdgeInsets.fromLTRB(
                                                defaultMargin,
                                                defaultMargin,
                                                defaultMargin,
                                                (dummyCategories.olahragaOutdoor
                                                            .indexOf(e) ==
                                                        dummyCategories
                                                                .olahragaOutdoor
                                                                .length -
                                                            1
                                                    ? defaultMargin
                                                    : 0),
                                              ),
                                              child: Text(
                                                e.name,
                                                style: blackTextFont,
                                              ),
                                            ),
                                          )
                                          .toList()
                                      : categoriesIndex == 8
                                          ? dummyCategories.bukuAlatTulis
                                              .map(
                                                (e) => Container(
                                                  margin: EdgeInsets.fromLTRB(
                                                    defaultMargin,
                                                    defaultMargin,
                                                    defaultMargin,
                                                    (dummyCategories
                                                                .bukuAlatTulis
                                                                .indexOf(e) ==
                                                            dummyCategories
                                                                    .bukuAlatTulis
                                                                    .length -
                                                                1
                                                        ? defaultMargin
                                                        : 0),
                                                  ),
                                                  child: Text(
                                                    e.name,
                                                    style: blackTextFont,
                                                  ),
                                                ),
                                              )
                                              .toList()
                                          : Container(),
        ),
      ),
    );
  }
}
