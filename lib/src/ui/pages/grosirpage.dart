part of 'pages.dart';

class GrosirPage extends StatefulWidget {
  final PageEvent pageEvent;

  const GrosirPage(this.pageEvent, {Key key}) : super(key: key);

  @override
  _GrosirPageState createState() => _GrosirPageState();
}

class _GrosirPageState extends State<GrosirPage> {
  TextEditingController minPemesananController = TextEditingController();
  TextEditingController maxPemesananController = TextEditingController();
  TextEditingController hargaSatuanController = TextEditingController();
  bool isSelected = false;
  int selectedAmount = 0;

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        context.read<PageBloc>().add(widget.pageEvent);
        return;
      },
      child: GeneralPage(
        title: "Grosir",
        onBackButtonPressed: () {
          context.read<PageBloc>().add(widget.pageEvent);
        },
        child: ListView(
          children: [
            Container(
              margin: EdgeInsets.all(defaultMargin),
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text("Min.\nPemesanan", style: blackTextFont),
                          SizedBox(height: 4),
                          Container(
                            height: 45,
                            width: MediaQuery.of(context).size.width / 4.8,
                            padding: EdgeInsets.symmetric(horizontal: 8),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(8),
                              border: Border.all(color: mainColor),
                            ),
                            child: TextField(
                              controller: minPemesananController,
                              keyboardType: TextInputType.number,
                              decoration:
                                  InputDecoration(border: InputBorder.none),
                            ),
                          ),
                        ],
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text("Max.\nPemesanan", style: blackTextFont),
                          SizedBox(height: 4),
                          Container(
                            height: 45,
                            width: MediaQuery.of(context).size.width / 4.8,
                            padding: EdgeInsets.symmetric(horizontal: 8),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(8),
                              border: Border.all(color: mainColor),
                            ),
                            child: TextField(
                              controller: maxPemesananController,
                              keyboardType: TextInputType.number,
                              decoration:
                                  InputDecoration(border: InputBorder.none),
                            ),
                          ),
                        ],
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text("Harga\nSatuan", style: blackTextFont),
                          SizedBox(height: 4),
                          Container(
                            height: 45,
                            width: MediaQuery.of(context).size.width / 2.2,
                            padding: EdgeInsets.symmetric(horizontal: 8),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(8),
                              border: Border.all(color: mainColor),
                            ),
                            child: TextField(
                              controller: hargaSatuanController,
                              keyboardType: TextInputType.number,
                              onChanged: (text) {
                                String temp = '';
                                for (int i = 0; i <= text.length; i++) {
                                  temp += text.isDigit(i) ? text[i] : '';
                                }
                                setState(() {
                                  selectedAmount = int.tryParse(temp) ?? 0;
                                });
                                hargaSatuanController.text =
                                    NumberFormat.currency(
                                            locale: 'id_ID',
                                            symbol: 'IDR ',
                                            decimalDigits: 0)
                                        .format(selectedAmount);
                                hargaSatuanController.selection =
                                    TextSelection.fromPosition(
                                  TextPosition(
                                      offset:
                                          hargaSatuanController.text.length),
                                );
                              },
                              decoration:
                                  InputDecoration(border: InputBorder.none),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                  SizedBox(height: 8),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Container(
                        width: MediaQuery.of(context).size.width / 2.2,
                        height: 40,
                        child: RaisedButton(
                          onPressed: () {},
                          elevation: 0,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(8)),
                          color: darkGreyColor,
                          child: Text(
                            'Tambah Harga Grosir',
                            style: blackTextFont,
                          ),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: 4),
                  Divider(thickness: 1, color: Colors.black)
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
