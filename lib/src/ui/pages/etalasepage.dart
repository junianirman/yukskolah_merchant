part of 'pages.dart';

class EtalasePage extends StatefulWidget {
  final PageEvent pageEvent;

  EtalasePage(this.pageEvent, {Key key}) : super(key: key);

  @override
  _EtalasePageState createState() => _EtalasePageState();
}

class _EtalasePageState extends State<EtalasePage> {
  TextEditingController etalaseController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        context.read<PageBloc>().add(widget.pageEvent);
        return;
      },
      child: GeneralPage(
        title: "Ongkos Kirim",
        onBackButtonPressed: () {
          context.read<PageBloc>().add(widget.pageEvent);
        },
        child: ListView(
          children: [
            Container(
              margin: EdgeInsets.all(defaultMargin),
              child: Column(
                children: [
                  Container(
                    height: 45,
                    width: MediaQuery.of(context).size.width,
                    padding: EdgeInsets.symmetric(horizontal: 10),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8),
                      border: Border.all(color: mainColor),
                    ),
                    child: TextField(
                      controller: etalaseController,
                      decoration: InputDecoration(border: InputBorder.none),
                    ),
                  ),
                  SizedBox(height: 8),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Container(
                        width: MediaQuery.of(context).size.width / 3.2,
                        height: 40,
                        child: RaisedButton(
                          onPressed: () {},
                          elevation: 0,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(8)),
                          color: darkGreyColor,
                          child: Text(
                            'Tambah Etalase',
                            style: blackTextFont,
                          ),
                        ),
                      ),
                    ],
                  ),
                  Divider(thickness: 1, color: Colors.black),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: dummyEtalase
                          .map(
                            (e) => Padding(
                              padding: EdgeInsets.symmetric(vertical: 4),
                              child: Row(
                                children: [
                                  Icon(Icons.circle,
                                      size: 14, color: darkGreyColor),
                                  SizedBox(width: 8),
                                  Text(e.name, style: blackTextFont),
                                ],
                              ),
                            ),
                          )
                          .toList(),
                    ),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
