part of 'pages.dart';

class SendPage extends StatelessWidget {
  final PageEvent pageEvent;

  const SendPage(this.pageEvent, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        context.read<PageBloc>().add(pageEvent);
        return;
      },
      child: GeneralPage(
        title: "Send",
        onBackButtonPressed: () {
          context.read<PageBloc>().add(pageEvent);
        },
        child: ListView(
          children: dummyTransaction
              .where((element) => element.status == "Terkirim")
              .toList()
              .map(
                (e) => Container(
                  margin: EdgeInsets.fromLTRB(
                    defaultMargin,
                    defaultMargin,
                    defaultMargin,
                    (dummyTransaction.indexOf(e) == dummyTransaction.length - 1
                        ? defaultMargin
                        : 0),
                  ),
                  height: MediaQuery.of(context).size.height / 1.6 - 28,
                  decoration: BoxDecoration(
                    color: greyColor,
                    borderRadius: BorderRadius.circular(8),
                  ),
                  child: SendCard(e),
                ),
              )
              .toList(),
        ),
      ),
    );
  }
}
