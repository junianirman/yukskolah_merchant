part of 'models.dart';

enum InboxType { school, loan, consultation, elearning, shopping }

class Inbox extends Equatable {
  final int id;
  final String picturePath;
  final String store;
  final String dateTime;
  final String chat;
  final InboxType inboxType;

  Inbox(
      {this.id,
      this.picturePath,
      this.store,
      this.dateTime,
      this.chat,
      this.inboxType});

  @override
  List<Object> get props => [id, picturePath, store, dateTime];
}

List<Inbox> dummyInbox = [
  Inbox(
      id: 1,
      picturePath: "assets/user_pic.jpg",
      store: "Alfa Store",
      dateTime: DateFormat('yyyy-MM-dd').format(DateTime.now()),
      chat: "hai, this stuff is ready?",
      inboxType: InboxType.shopping),
  Inbox(
      id: 2,
      picturePath: "assets/user_pic.jpg",
      store: "Betta Store",
      dateTime: DateFormat('yyyy-MM-dd').format(DateTime.now()),
      chat: "can be delivered today?",
      inboxType: InboxType.shopping)
];
