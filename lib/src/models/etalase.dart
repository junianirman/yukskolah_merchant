part of 'models.dart';

class Etalase extends Equatable {
  final int id;
  final String name;

  Etalase({this.id, this.name});

  @override
  List<Object> get props => [id, name];
}

List<Etalase> dummyEtalase = [
  Etalase(id: 1, name: "Sepatu"),
  Etalase(id: 2, name: "Seragam"),
];
