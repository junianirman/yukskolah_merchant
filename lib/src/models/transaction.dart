part of 'models.dart';

enum OrderTab { dikemas, dikirim, selesai, pembatalan, pengembalian }

class Transaction extends Equatable {
  final int id;
  final int quantity;
  final int shippingCost;
  final int total;
  final String status;
  final String orderNumber;
  final String deliveryService;
  final String receipt;
  final DateTime orderDate;
  final Buyer buyer;
  final Product product;
  final Complain complain;
  final Cancel cancel;
  final OrderTab orderTab;

  Transaction({
    this.id,
    this.quantity,
    this.shippingCost,
    this.total,
    this.status,
    this.orderNumber,
    this.deliveryService,
    this.receipt,
    this.orderDate,
    this.buyer,
    this.product,
    this.complain,
    this.cancel,
    this.orderTab,
  });

  @override
  List<Object> get props => [
        id,
        quantity,
        shippingCost,
        total,
        status,
        orderNumber,
        deliveryService,
        receipt,
        orderDate,
        buyer,
        product,
        complain,
        cancel,
        orderTab
      ];
}

List<Transaction> dummyTransaction = [
  Transaction(
    id: 1,
    quantity: 1,
    shippingCost: 15000,
    total: 135000,
    status: "Terkirim",
    orderNumber: "#202334DZQA7NWE",
    deliveryService: "Sicepat Regular",
    receipt: "SCP235658779989",
    orderDate: DateTime.now(),
    buyer: dummyBuyer,
    product: dummyProduct[0],
    complain: dummyComplain,
  ),
  Transaction(
    id: 2,
    quantity: 1,
    shippingCost: 15000,
    total: 135000,
    status: "Sudah Bayar",
    orderNumber: "#202334DZQA7NWE",
    deliveryService: "Sicepat Regular",
    orderDate: DateTime.now(),
    buyer: dummyBuyer,
    product: dummyProduct[0],
    orderTab: OrderTab.dikemas,
  ),
  Transaction(
    id: 3,
    quantity: 1,
    shippingCost: 15000,
    total: 135000,
    status: "Sudah Bayar",
    orderNumber: "#202334DZQA7NWE",
    deliveryService: "Sicepat Regular",
    orderDate: DateTime.now(),
    buyer: dummyBuyer,
    product: dummyProduct[0],
    orderTab: OrderTab.dikirim,
  ),
  Transaction(
    id: 4,
    quantity: 1,
    shippingCost: 15000,
    total: 135000,
    receipt: "SCP235658779989",
    status: "Sudah Bayar",
    orderNumber: "#202334DZQA7NWE",
    deliveryService: "Sicepat Regular",
    orderDate: DateTime.now(),
    buyer: dummyBuyer,
    product: dummyProduct[0],
    orderTab: OrderTab.selesai,
  ),
  Transaction(
    id: 5,
    quantity: 1,
    shippingCost: 15000,
    total: 135000,
    receipt: "SCP235658779989",
    status: "Sudah Bayar",
    orderNumber: "#202334DZQA7NWE",
    deliveryService: "Sicepat Regular",
    orderDate: DateTime.now(),
    buyer: dummyBuyer,
    product: dummyProduct[0],
    cancel: dummyCancel,
    orderTab: OrderTab.pembatalan,
  ),
  Transaction(
    id: 5,
    quantity: 1,
    shippingCost: 15000,
    total: 135000,
    status: "Sudah Bayar",
    orderNumber: "#202334DZQA7NWE",
    deliveryService: "Sicepat Regular",
    orderDate: DateTime.now(),
    buyer: dummyBuyer,
    product: dummyProduct[0],
    cancel: dummyCancel,
    complain: dummyComplain,
    orderTab: OrderTab.pengembalian,
  ),
];
