part of 'models.dart';

class Business extends Equatable {
  final int id;
  final int pesanan;
  final int presentasePesanan;
  final int penjualan;
  final int presentasePenjualan;
  final int tingkatKonversi;
  final int presentaseTingkatKonversi;
  final int penjualanPerpesanan;
  final int presentasePenjualanPerpesanan;
  final int totalPengunjung;
  final int presentaseTotalPengunjung;
  final int produkDilihat;
  final int presentaseProdukDilihat;

  Business({
    this.id,
    this.pesanan,
    this.presentasePesanan,
    this.penjualan,
    this.presentasePenjualan,
    this.tingkatKonversi,
    this.presentaseTingkatKonversi,
    this.penjualanPerpesanan,
    this.presentasePenjualanPerpesanan,
    this.totalPengunjung,
    this.presentaseTotalPengunjung,
    this.produkDilihat,
    this.presentaseProdukDilihat,
  });

  @override
  List<Object> get props => [
        id,
        pesanan,
        presentasePesanan,
        penjualan,
        presentasePenjualan,
        tingkatKonversi,
        presentaseTingkatKonversi,
        penjualanPerpesanan,
        presentasePenjualanPerpesanan,
        totalPengunjung,
        presentaseTotalPengunjung,
        produkDilihat,
        presentaseProdukDilihat,
      ];
}

Business dummyBusiness = Business(
    id: 1,
    pesanan: 0,
    presentasePesanan: 0,
    penjualan: 0,
    presentasePenjualan: 0,
    tingkatKonversi: 0,
    presentaseTingkatKonversi: 0,
    penjualanPerpesanan: 0,
    presentasePenjualanPerpesanan: 0,
    totalPengunjung: 10,
    presentaseTotalPengunjung: 0,
    produkDilihat: 13,
    presentaseProdukDilihat: 0);
