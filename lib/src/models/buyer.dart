part of 'models.dart';

class Buyer extends Equatable {
  final int id;
  final String name;
  final String email;
  final String address;
  final String houseNumber;
  final String phoneNumber;
  final String city;
  final String picturePath;

  Buyer({
    this.id,
    this.name,
    this.email,
    this.address,
    this.houseNumber,
    this.phoneNumber,
    this.city,
    this.picturePath,
  });

  @override
  List<Object> get props => [
        id,
        name,
        email,
        address,
        houseNumber,
        phoneNumber,
        city,
        picturePath,
      ];
}

Buyer dummyBuyer = Buyer(
  id: 1,
  name: 'Fajar',
  address:
      'Jl. Cilandak 47, No. 889, Rt007/008, Cilandak, Pasar Minggu, Jakarta Selatan, DKI Jakarta, 12545',
  city: 'Jakarta',
  houseNumber: '889',
  phoneNumber: '081223456789',
  email: 'dokter@gmail.com',
  picturePath: 'assets/user_pic.jpg',
);
