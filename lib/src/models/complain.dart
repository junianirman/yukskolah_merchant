part of 'models.dart';

class Complain extends Equatable {
  final int id;
  final String complain;

  Complain({this.id, this.complain});

  @override
  List<Object> get props => [id, complain];
}

Complain dummyComplain = Complain(id: 1, complain: "Ukurannya salah kirim");
