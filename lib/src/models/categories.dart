part of 'models.dart';

class Categories extends Equatable {
  final int id;
  final List<FoodBeverage> foodBeverage;
  final List<FashionMuslim> fashionMuslim;
  final List<PakaianPria> pakaianPria;
  final List<PakaianWanita> pakaianWanita;
  final List<FashionAnak> fashionAnak;
  final List<KomputerAksesoris> komputerAksesoris;
  final List<OlahragaOutdoor> olahragaOutdoor;
  final List<BukuAlatTulis> bukuAlatTulis;

  Categories({
    this.id,
    this.foodBeverage,
    this.fashionMuslim,
    this.pakaianPria,
    this.pakaianWanita,
    this.fashionAnak,
    this.komputerAksesoris,
    this.olahragaOutdoor,
    this.bukuAlatTulis,
  });

  @override
  List<Object> get props => [id];
}

Categories dummyCategories = Categories(
  id: 1,
  foodBeverage: dummyFoodBeverages,
  fashionMuslim: dummyFashionMuslim,
  pakaianPria: dummyPakaianPria,
  pakaianWanita: dummyPakaianWanita,
  fashionAnak: dummyFashionAnak,
  komputerAksesoris: dummyKomputerAksesoris,
  olahragaOutdoor: dummyOlahragaOutdoor,
  bukuAlatTulis: dummyBukuAlatTulis,
);

class FoodBeverage extends Equatable {
  final int id;
  final String name;

  FoodBeverage({this.id, this.name});

  @override
  List<Object> get props => [id, name];
}

List<FoodBeverage> dummyFoodBeverages = [
  FoodBeverage(id: 1, name: "Bahan Kue"),
  FoodBeverage(id: 2, name: "Makanan Ringan"),
  FoodBeverage(id: 3, name: "Menu Sarapan"),
  FoodBeverage(id: 4, name: "Produk Susu"),
  FoodBeverage(id: 5, name: "Catering"),
  FoodBeverage(id: 6, name: "Lainnya"),
];

class FashionMuslim extends Equatable {
  final int id;
  final String name;

  FashionMuslim({this.id, this.name});

  @override
  List<Object> get props => [id, name];
}

List<FashionMuslim> dummyFashionMuslim = [
  FashionMuslim(id: 1, name: "Aksesoris Muslim"),
  FashionMuslim(id: 2, name: "Jilbab"),
  FashionMuslim(id: 3, name: "Pakaian Muslim Anak"),
  FashionMuslim(id: 4, name: "Baju Renang Muslim"),
  FashionMuslim(id: 5, name: "Perlengkapan Ibadah"),
  FashionMuslim(id: 6, name: "Lainnya"),
];

class PakaianPria extends Equatable {
  final int id;
  final String name;

  PakaianPria({this.id, this.name});

  @override
  List<Object> get props => [id, name];
}

List<PakaianPria> dummyPakaianPria = [
  PakaianPria(id: 1, name: "Aksesoris Pria"),
  PakaianPria(id: 2, name: "Sepatu Pria"),
  PakaianPria(id: 3, name: "Tas Pria"),
  PakaianPria(id: 4, name: "Seragam Pria"),
  PakaianPria(id: 5, name: "Batik Pria"),
  PakaianPria(id: 6, name: "Lainnya"),
];

class PakaianWanita extends Equatable {
  final int id;
  final String name;

  PakaianWanita({this.id, this.name});

  @override
  List<Object> get props => [id, name];
}

List<PakaianWanita> dummyPakaianWanita = [
  PakaianWanita(id: 1, name: "Aksesoris Wanita"),
  PakaianWanita(id: 2, name: "Sepatu Wanita"),
  PakaianWanita(id: 3, name: "Tas Wanita"),
  PakaianWanita(id: 4, name: "Seragam Wanita"),
  PakaianWanita(id: 5, name: "Batik Wanita"),
  PakaianWanita(id: 6, name: "Lainnya"),
];

class FashionAnak extends Equatable {
  final int id;
  final String name;

  FashionAnak({this.id, this.name});

  @override
  List<Object> get props => [id, name];
}

List<FashionAnak> dummyFashionAnak = [
  FashionAnak(id: 1, name: "Aksesoris Anak"),
  FashionAnak(id: 2, name: "Sepatu Anak"),
  FashionAnak(id: 3, name: "Tas Anak"),
  FashionAnak(id: 4, name: "Seragam Anak"),
  FashionAnak(id: 5, name: "Batik Anak"),
  FashionAnak(id: 6, name: "Lainnya"),
];

class KomputerAksesoris extends Equatable {
  final int id;
  final String name;

  KomputerAksesoris({this.id, this.name});

  @override
  List<Object> get props => [id, name];
}

List<KomputerAksesoris> dummyKomputerAksesoris = [
  KomputerAksesoris(id: 1, name: "Monitor"),
  KomputerAksesoris(id: 2, name: "RAM"),
  KomputerAksesoris(id: 3, name: "Harddisk"),
  KomputerAksesoris(id: 4, name: "CPU"),
  KomputerAksesoris(id: 5, name: "Laptop"),
  KomputerAksesoris(id: 6, name: "Flashdisk"),
  KomputerAksesoris(id: 7, name: "Keyboard"),
  KomputerAksesoris(id: 8, name: "Mouse"),
  KomputerAksesoris(id: 9, name: "Lainnya"),
];

class OlahragaOutdoor extends Equatable {
  final int id;
  final String name;

  OlahragaOutdoor({this.id, this.name});

  @override
  List<Object> get props => [id, name];
}

List<OlahragaOutdoor> dummyOlahragaOutdoor = [
  OlahragaOutdoor(id: 1, name: "Aksesoris Olahraga"),
  OlahragaOutdoor(id: 2, name: "Camping Hiking"),
  OlahragaOutdoor(id: 3, name: "Perlengkapan Lari"),
  OlahragaOutdoor(id: 4, name: "Perlengkapan Renang"),
  OlahragaOutdoor(id: 5, name: "Perlengkapan Bola"),
  OlahragaOutdoor(id: 6, name: "Perlengkapan Bulutangkis"),
  OlahragaOutdoor(id: 7, name: "Perlengkapan Volly"),
  OlahragaOutdoor(id: 8, name: "Perlengkapan Basket"),
  OlahragaOutdoor(id: 9, name: "Lainnya"),
];

class BukuAlatTulis extends Equatable {
  final int id;
  final String name;

  BukuAlatTulis({this.id, this.name});

  @override
  List<Object> get props => [id, name];
}

List<BukuAlatTulis> dummyBukuAlatTulis = [
  BukuAlatTulis(id: 1, name: "Buku Komputer"),
  BukuAlatTulis(id: 2, name: "Buku Matematika"),
  BukuAlatTulis(id: 3, name: "Buku Biologi"),
  BukuAlatTulis(id: 4, name: "Buku IPA"),
  BukuAlatTulis(id: 5, name: "Buku IPS"),
  BukuAlatTulis(id: 6, name: "Buku Otomotif"),
  BukuAlatTulis(id: 7, name: "Buku Ekonomi"),
  BukuAlatTulis(id: 8, name: "Buku Bahasa Inggris"),
  BukuAlatTulis(id: 9, name: "Buku Bahasa Indonesia"),
  BukuAlatTulis(id: 10, name: "Buku Musik"),
  BukuAlatTulis(id: 11, name: "Lainnya"),
];
