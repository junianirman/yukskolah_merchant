part of 'models.dart';

class Sales extends Equatable {
  final int id;
  final int confirm;
  final int process;
  final int send;
  final int complain;
  final int delivered;
  final int cancel;

  Sales({
    this.id,
    this.confirm,
    this.process,
    this.send,
    this.complain,
    this.delivered,
    this.cancel,
  });

  @override
  List<Object> get props =>
      [id, confirm, process, send, complain, delivered, cancel];
}

Sales dummySales = Sales(
  id: 1,
  confirm: 50,
  process: 2,
  send: 1,
  complain: 1,
  delivered: 47,
  cancel: 1,
);
