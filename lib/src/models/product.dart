part of 'models.dart';

enum ProductTab { live, habis, diblokir, diarsipkan }

class Product extends Equatable {
  final int id;
  final String picturePath;
  final String name;
  final String description;
  final int price;
  final List<Varian> varian;
  final List<int> size;
  final List<String> color;
  final int discount;
  final String promoCode;
  final int stock;
  final int favourite;
  final int sold;
  final int seen;
  final ProductTab productTab;

  Product({
    this.id,
    this.picturePath,
    this.name,
    this.description,
    this.price,
    this.varian,
    this.size,
    this.color,
    this.discount,
    this.promoCode,
    this.stock,
    this.favourite,
    this.sold,
    this.seen,
    this.productTab,
  });

  @override
  List<Object> get props => [
        id,
        picturePath,
        name,
        description,
        price,
        varian,
        size,
        color,
        discount,
        promoCode,
        stock,
        favourite,
        sold,
        seen,
        productTab,
      ];
}

List<Product> dummyProduct = [
  Product(
    id: 1,
    picturePath: "assets/shoes_1.jpg",
    name: "Sepatu Baru",
    description: "",
    price: 120000,
    varian: [
      Varian(picturePath: "assets/shoes_1.jpg"),
      Varian(picturePath: "assets/shoes_3.jpg")
    ],
    size: [40, 41, 42],
    color: ["Merah", "Kuning", "Hijau", "Biru", "Hitam"],
    stock: 0,
    favourite: 5,
    sold: 10,
    seen: 15,
    productTab: ProductTab.habis,
  ),
  Product(
    id: 2,
    picturePath: "assets/shoes_2.jpg",
    name: "Model ABCD",
    description: "",
    price: 120000,
    varian: [
      Varian(picturePath: "assets/shoes_1.jpg"),
      Varian(picturePath: "assets/shoes_3.jpg")
    ],
    size: [40, 41, 42],
    color: ["Merah", "Kuning", "Hijau", "Biru", "Hitam"],
    stock: 2,
    favourite: 0,
    sold: 1,
    seen: 10,
    productTab: ProductTab.live,
  ),
];
