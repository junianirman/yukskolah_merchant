part of 'models.dart';

class ProductTabMenu extends Equatable {
  final String title;
  final int sumProduct;

  ProductTabMenu({this.title, this.sumProduct});

  @override
  List<Object> get props => [title, sumProduct];
}

List<ProductTabMenu> dummyProductTabMenu = [
  ProductTabMenu(
      title: "Live",
      sumProduct: dummyProduct
          .where((element) => element.productTab == ProductTab.live)
          .toList()
          .length),
  ProductTabMenu(
      title: "Habis",
      sumProduct: dummyProduct
          .where((element) => element.productTab == ProductTab.habis)
          .toList()
          .length),
  ProductTabMenu(
      title: "Diblokir",
      sumProduct: dummyProduct
          .where((element) => element.productTab == ProductTab.diblokir)
          .toList()
          .length),
  ProductTabMenu(
      title: "Diarsipkan",
      sumProduct: dummyProduct
          .where((element) => element.productTab == ProductTab.diarsipkan)
          .toList()
          .length)
];
