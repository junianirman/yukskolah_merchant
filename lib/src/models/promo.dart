part of 'models.dart';

enum PromoType { presentase, rupiah }

class Promo extends Equatable {
  final int id;
  final String promoCode;
  final double discount;
  final int quota;
  final DateTime expiredDate;
  final PromoType promoType;

  Promo({
    this.id,
    this.promoCode,
    this.discount,
    this.quota,
    this.expiredDate,
    this.promoType,
  });

  @override
  List<Object> get props => [id, promoCode, discount, quota, expiredDate];
}

List<Promo> dummyPromo = [
  Promo(
    id: 1,
    promoCode: "AXDEFRW",
    discount: 10000,
    quota: 50,
    expiredDate: DateTime.now(),
    promoType: PromoType.rupiah,
  ),
  Promo(
    id: 2,
    promoCode: "CCDREFW",
    discount: 5,
    quota: 50,
    expiredDate: DateTime.now(),
    promoType: PromoType.presentase,
  ),
];
