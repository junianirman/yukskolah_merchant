part of 'models.dart';

class Varian extends Equatable {
  final String picturePath;

  Varian({this.picturePath});

  @override
  List<Object> get props => [picturePath];
}
