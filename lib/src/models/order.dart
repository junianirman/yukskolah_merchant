part of 'models.dart';

class Order extends Equatable {
  final int id;
  final String status;
  final Buyer buyer;
  final Product product;
  final OrderTab orderTab;

  Order({this.id, this.status, this.buyer, this.product, this.orderTab});

  @override
  List<Object> get props => [id, product, status, orderTab];
}

List<Order> dummyOrder = [
  Order(
    id: 1,
    status: "Sudah Bayar",
    buyer: dummyBuyer,
    product: dummyProduct[0],
    orderTab: OrderTab.dikemas,
  ),
];
