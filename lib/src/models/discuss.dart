part of 'models.dart';

class Discuss extends Equatable {
  final int id;
  final String discuss;
  final int quantity;
  final DateTime discussDate;
  final Buyer buyer;
  final Product product;

  Discuss(
      {this.id,
      this.discuss,
      this.quantity,
      this.discussDate,
      this.buyer,
      this.product});

  @override
  List<Object> get props =>
      [id, discuss, quantity, discussDate, buyer, product];
}

List<Discuss> dummyDiscuss = [
  Discuss(
    id: 1,
    discuss: "Apakah stok ada?",
    quantity: 1,
    discussDate: DateTime.now(),
    buyer: dummyBuyer,
    product: dummyProduct[0],
  ),
];
