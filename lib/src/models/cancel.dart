part of 'models.dart';

class Cancel extends Equatable {
  final int id;
  final String description;
  final DateTime cancelDate;

  Cancel({this.id, this.description, this.cancelDate});

  @override
  List<Object> get props => [id, description, cancelDate];
}

Cancel dummyCancel = Cancel(
  id: 1,
  description: "Pemesanan di batalkan",
  cancelDate: DateTime.now(),
);
