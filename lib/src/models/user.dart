part of 'models.dart';

class User extends Equatable {
  final int id;
  final String name;
  final String dateOfBirth;
  final String email;
  final String address;
  final String houseNumber;
  final String phoneNumber;
  final String city;
  final String picturePath;
  final int balance;
  final Follow follow;

  User({
    this.id,
    this.name,
    this.dateOfBirth,
    this.email,
    this.address,
    this.houseNumber,
    this.phoneNumber,
    this.city,
    this.picturePath,
    this.balance,
    this.follow,
  });

  @override
  List<Object> get props => [
        id,
        name,
        dateOfBirth,
        email,
        address,
        houseNumber,
        phoneNumber,
        city,
        picturePath,
        balance,
        follow
      ];
}

User dummyUser = User(
  id: 1,
  name: 'Toko Sepatu Serba Ada',
  dateOfBirth: '08-01-1990',
  address:
      'Jl. Cilandak 47, No. 889, Rt007/008, Cilandak, Pasar Minggu, \nJakarta Selatan, DKI Jakarta, 12545',
  city: 'Jakarta',
  houseNumber: '889',
  phoneNumber: '081223456789',
  email: 'dokter@gmail.com',
  picturePath: 'assets/user_pic.jpg',
  balance: 512000,
  follow: Follow(follower: 250, following: 2),
);
